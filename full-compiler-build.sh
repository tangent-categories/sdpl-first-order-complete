#!/bin/bash

# Update the list of cabal packages 
./sync-cabal.sh

# This runs the frontend generator
./build-bnfc-frontend.sh

# Then runs the backend generator
./build-bnfc-backend.sh

# Then builds the compiler
./build-compiler.sh