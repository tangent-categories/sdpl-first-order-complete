module Join where

{-
With the bound syntax we're using, we end up with a lot of unitless monads 
or semimonads, or whatever they're called.
-}

class Functor m => Join m where 
    {- |
    An instance of join must have both paths from m (m (m a)) to m a equal.  
    That is, 
        squish . squish == squish . fmap squish
    -}
    squish :: m (m a) -> m a