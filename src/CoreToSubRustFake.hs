{- |
Incremental backend.  Not meant to be fully working. 
Used for sandboxed playing just to test things out.

Currently, this is used to compile down terms without typing annotations given a very 
strict form for the terms.
-}
module CoreToSubRustFake where

import CoreSyntax
import SubRust.Abs

import Data.Text (Text)
import qualified Data.Text as T

coreToSubRustProg :: CoreProg a -> Prog' a
coreToSubRustProg (CoreProgram a cds) = Program a (map coreToSubRustDecl cds)

coreToSubRustDecl :: CoreDecl a -> Decl' a
coreToSubRustDecl x = case x of
  CoreConstBinding a cpi ct -> FunBinding a (cpiToPI cpi) (Concrete a []) (OutputTraits a (RF64 a) []) (coreToSubRustTerm ct)
  CoreFunBinding a cpi cpis ct ->
    let
        rustInputs =  Concrete a $ cpisToInputDecl cpis a
    in
        FunBinding a (cpiToPI cpi) rustInputs (OutputTraits a (RF64 a) []) (coreToSubRustTerm ct)
  InternalNamedDiff a dn cpis cpis2 ct ->
    let
        rustInputs = Concrete a $ cpisToInputDecl (cpis ++ cpis2) a
    in
        FunBinding a (diffNameToPI dn) rustInputs (OutputTraits a (RF64 a) []) (coreToSubRustTerm ct)

diffNameToPI :: DiffName -> PIdent
diffNameToPI (D nat (CorePIdent (x1, txt))) = PIdent (x1, dnName)
    where dnName = T.append (T.append (T.pack "__") (T.pack (show (fromEnum nat)))) txt

cpisToInputDecl :: [CorePIdent] -> a -> [ArgType' a]
cpisToInputDecl cpis a = map (\cp -> TypedVariable a (cpiToPI cp) (RF64 a)) cpis

coreToSubRustTerm :: CoreTerm a -> Term' a
coreToSubRustTerm x = case x of
  CorePlus a ct ct' -> RPlus a (coreToSubRustTerm ct) (coreToSubRustTerm ct')
  CoreZero a -> RZero a
  CoreScalar a ct ct' -> RScalar a (coreToSubRustTerm ct) (coreToSubRustTerm ct')
  CoreDot a ct ct' -> RDot a (coreToSubRustTerm ct) (coreToSubRustTerm ct')
  CoreUnit a -> RUTerm a
  CorePair a ct ct' -> RPairTerm a (coreToSubRustTerm ct) (coreToSubRustTerm ct')
  CoreSin a ct -> RSin a (coreToSubRustTerm ct)
  CoreCos a ct -> RCos a (coreToSubRustTerm ct)
  CoreExp a ct -> RExp a (coreToSubRustTerm ct)
  CoreFst a ct -> RFst a (coreToSubRustTerm ct)
  CoreSnd a ct -> RSnd a (coreToSubRustTerm ct)
  CoreVar a cpi -> RVar a (cpiToPI cpi)
  CoreFunCall a cpi cts -> RCall a (cpiToPI cpi) (map coreToSubRustTerm cts)
  InternalDiffFunCall a dn cts cts2 -> RCall a (diffNameToPI dn) (map coreToSubRustTerm (cts ++ cts2))
  CoreConst a (CoreNumber x1) -> RConst a (Number x1)
  CoreWhen a cbts -> error "undefined"
  CoreLet a x m n  -> BlockTerm a [AsstStmt a (cpiToPI x) (coreToSubRustTerm m)] (coreToSubRustTerm n)
  CoreBlockTerm a css ct -> BlockTerm a (coreToSubRustStmts css) (coreToSubRustTerm ct)

coreToSubRustStmts :: [CoreStmt a] -> [Stmt' a]
coreToSubRustStmts = map coreToSubRustStmt

coreToSubRustStmt :: CoreStmt a -> Stmt' a
coreToSubRustStmt stmt = case stmt of 
  CoreDeclStmt a cd -> case cd of
    (CoreConstBinding a' cpi ct) -> AsstStmt a (cpiToPI cpi) (coreToSubRustTerm ct)
    (CoreFunBinding a' cpi cpis ct) -> error "not yet defined"
    (InternalNamedDiff a' dn cpis cpis' ct) -> error "not yet defined"
  CoreWhile a cbt css -> WhileStmt a (coreToSubRustBool cbt) (coreToSubRustStmts css)

coreToSubRustBool :: CoreBoolTerm a -> BTerm' a
coreToSubRustBool b = case b of 
  CoreOr a cbt cbt' -> ROr a (coreToSubRustBool cbt) (coreToSubRustBool cbt')
  CoreAnd a cbt cbt' -> RAnd a (coreToSubRustBool cbt) (coreToSubRustBool cbt')
  CoreNot a cbt -> RNot a (coreToSubRustBool cbt)
  CoreLess a ct ct' -> RLess a (coreToSubRustTerm ct) (coreToSubRustTerm ct')
  CoreGreater a ct ct' -> RGreater a (coreToSubRustTerm ct) (coreToSubRustTerm ct')





cpiToPI :: CorePIdent -> PIdent
cpiToPI (CorePIdent x1) = PIdent x1

