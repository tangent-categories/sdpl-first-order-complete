{-# LANGUAGE FlexibleContexts #-}
module AbsToBoundAbs where 

import Language.Abs 
import BoundAbs

import Bound


import qualified Data.Text.Read as Read 
import CompilerError
import Utils ( listToMap )
import Data.Text (Text)
import TermUtils (pIdentToText)

{-
Take a program and convert it to a bound program.  
In a bound program, note all variables represent nullary function calls.
-}
-- progToBoundProg :: Prog -> BProg (Maybe ()) ((Int,Int),Text)
progToBoundProg p = case p of 
    Program _ des -> BProg (map funToBoundFun des)

-- funToBoundFun :: Decl -> BFunction (Maybe ()) ((Int,Int),Text)
funToBoundFun f = case f of 
  ConstBinding ma (PIdent ((l,r),name)) te -> do 
      te' <- makeScopedBody [] te
      return $ BFunction (l,r,name) () te'
  FunBinding ma (PIdent ((l,r),name)) pis te -> do 
      te' <- makeScopedBody pis te
      return $ BFunction (l,r,name) () te'

-- makeScopedBody :: [PIdent] -> Term' BNFC'Position -> Scope Int (BTerm (Maybe ())) ((Int, Int), Text)
makeScopedBody [] t = do 
    t' <- termToBoundTerm t
    return $ abstract (const Nothing) t' 
makeScopedBody args t = do 
    t' <- termToBoundTerm t 
    return $ abstract (listToMap args') t'
        where args' = map (\(PIdent m) -> m) args

-- termToBoundTerm :: Term' BNFC'Position -> BTerm (Maybe ()) ((Int, Int), Text)
termToBoundTerm t = case t of 
  Plus _ te te' -> do 
      te1 <- termToBoundTerm te
      te2 <- termToBoundTerm te'
      return $ BPlus te1 te2
  Zero _ -> return BZero
  Scalar _ te te' -> do 
      te1 <- termToBoundTerm te
      te2 <- termToBoundTerm te'
      return $ BScalar te1 te2
  Dot _ te te' -> 
      do 
      te1 <- termToBoundTerm te
      te2 <- termToBoundTerm te'
      return $ BDot te1 te2
  Unit _ -> return BUnit 
  Pair _ te te' -> do 
      te1 <- termToBoundTerm te
      te2 <- termToBoundTerm te'
      return $ BPair te1 te2
  Sin _ te -> do 
      te' <- termToBoundTerm te
      return $ BSin te'
  Cos _ te -> do 
      te' <- termToBoundTerm te
      return $ BCos te'
  Exp _ te -> do 
      te' <- termToBoundTerm te
      return $ BExp te'
  Fst _ te -> do 
      te' <- termToBoundTerm te
      return $ BFst te'
  Snd _ te -> do 
      te' <- termToBoundTerm te
      return $ BSnd te'
  Var _ (PIdent pi) -> return $ BVar pi
  FunCall _ pi tes -> do 
      let name = pIdentToText pi
      tes' <- mapM termToBoundTerm tes
      return $ BFunCall name tes'
  Const _ (Number ((l,r),n)) -> do 
      n' <- tryReadNum n
      return $ BConst ((l,r), n') 
  When _ bts -> do 
      bts' <- mapM bThenToBoundBThen bts
      return $ BWhen bts'
  -- Let x m n == n[m/x] == let x = m in n
  Let _ (PIdent pi) m n -> do 
      m' <- termToBoundTerm m
      n' <- termToBoundTerm n
      let nBound = abstract1 pi n' 
      return $ BLet m' nBound
  BlockTerm _ sts te -> do 
      sts' <- mapM stmtToBoundStmt sts 
      te' <- termToBoundTerm te
      return $ BBlock sts' te'
  DiffTerm _ m xs bs vs -> do 
      m' <- termToBoundTerm m 
      bs' <- mapM termToBoundTerm bs 
      vs' <- mapM termToBoundTerm vs 
      let xs' = map (\(PIdent loc) -> loc ) xs
      -- make bound 
      let mBound = abstract (listToMap xs') m'
      return $ BDiffTerm (length xs') mBound bs' vs'

-- stmtToBoundStmt :: Stmt' a -> m (BStmt b ((Int, Int), Text))
stmtToBoundStmt s = case s of 
  DeclStmt _ de -> do 
      de' <- funToBoundFun de
      return $ BLocalDef de'
  While _ bt sts -> do 
      bt' <- boolToBoundBool bt 
      sts' <- mapM stmtToBoundStmt sts 
      return $ BWhile bt' sts'


-- bThenToBoundBThen :: (Monad m) => BThen -> m (BBThen b ((Int, Int), Text))
bThenToBoundBThen t = case t of 
    Guard _ bt te -> do 
        bt' <- boolToBoundBool bt 
        te' <- termToBoundTerm te 
        return $ BGuard bt' te'

-- boolToBoundBool ::(Monad m) => BoolTerm' BNFC'Position -> m (BBoolTerm b ((Int, Int), Text))
boolToBoundBool t = case t of 
  Or _ bt bt' -> do 
      bt1 <- boolToBoundBool bt 
      bt2 <- boolToBoundBool bt'
      return $ BOr bt1 bt2
  And _ bt bt' -> do 
      bt1 <- boolToBoundBool bt 
      bt2 <- boolToBoundBool bt' 
      return $ BAnd bt1 bt2
  Not _ bt -> do 
      bt' <- boolToBoundBool bt
      return $ BNot bt'
  Less _ te te' -> do 
      t1 <- termToBoundTerm te 
      t2 <- termToBoundTerm te' 
      return $ BLess t1 t2  
  Greater _ te te' -> do 
      t1 <- termToBoundTerm te 
      t2 <- termToBoundTerm te' 
      return $ BGreater t1 t2



tryReadNum n = case Read.rational n of 
    Left s -> throwBound s
    Right (n', rest) -> return n'

