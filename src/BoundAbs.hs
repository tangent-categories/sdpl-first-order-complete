-- {-# LANGUAGE DeriveFunctor #-}
-- {-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE DeriveTraversable #-}


{-
BoundTerms uses a locally nameless representation of terms ala 
Edward Kmett's bound variable library.  It makes tasks like 
type inference and differentiation less mentally exhausting 
while also providing some nice utilities for introducing 
additional bound variables as needed in various places.
-}

module BoundAbs where

import Bound
import Control.Monad

import Data.Text (Text)
import Data.Bifunctor
import Control.Monad.Morph (MFunctor(hoist))

import Data.Bifoldable
import Bound.Scope (foldMapScope, bitraverseScope)
import Bound.Var (unvar)
import Data.Bitraversable
-- import Data.Bitraversable (Bitraversable,bisequenceA)

-- import qualified Data.Sequence as Seq
-- import qualified Prelude as Seq
-- import Data.Sequence (Seq)


newtype BProg b a = BProg [BFunction b a] deriving (Functor,Foldable,Traversable)

instance Bifunctor BProg where
  -- second = fmap
  -- first f (BProg fns) = BProg (map (first f) fns)
  bimap = bimapDefault

instance Bifoldable BProg where
  -- bifoldMap f g (BProg fns)= foldr (mappend . bifoldMap f g) mempty fns
  bifoldMap = bifoldMapDefault

instance Bitraversable BProg where
  -- bitraverse :: (a -> f c) -> (b -> f d) -> BProg a b -> f (BProg c d)
  -- fns :: [BFunction a b]
  -- map (bitraverse f g) fns :: [f (BFunction c d)]
  -- sequenceA (map bisequence fns) :: f [BFunction a b]
  -- BProg :: [BFunction a b] -> BProg a b
  -- pure BProg :: f ([BFunction a b] -> BProg a b)
  bitraverse f g (BProg fns) = BProg <$> traverse (bitraverse f g) fns

-- A function is given by the name of the function and  binds its arguments 
-- in a term.  Since the term is locally nameless, we need only record 
-- its number of arguments.  The b argument is used to record things like typing information.
-- Inside the scope, we record the number (n) of bound variables (they are then 0--n).
data BFunction b a = BFunction (Int,Int,Text) !b (Scope Int (BTerm b)  a) deriving (Functor,Foldable,Traversable)

instance Bifunctor BFunction where
  -- second = fmap
  -- first f (BFunction name b body) = BFunction name (f b) (hoist (first f) body)
  bimap = bimapDefault   

instance Bifoldable BFunction where
  -- bifoldMap f g (BFunction name sig body) =
  --   let
  --     bodyUnScoped = fromScope body
  --     -- g :: b -> m, but to continue on, we need to get a Var Int b -> m. 
  --     -- However, the Var library has a function called unvar :: (c -> r) -> (d -> r) -> Var c d -> r which is the thing
  --     -- we need if we apply to the constantly mempty function for the Int, and g for the b.
  --     bodyBifolded = bifoldMap f (unvar (const mempty) g) bodyUnScoped
  --   in
  --     -- finally we combine the bodyBifolded with f sig.
  --     mappend (f sig) bodyBifolded
  bifoldMap  = bifoldMapDefault   

instance Bitraversable BFunction where
  -- f :: a -> f c
  -- g :: b -> f d
  -- bitraverseScope f g body :: f (Scope Int (BTerm c) d)
  -- f sig :: f c
  -- BFunction name :: a -> Scope Int (BTerm a) b -> BFunction a b
  -- pure (BFunction name) :: f (a -> ...)
  bitraverse f g (BFunction name sig body) =
    let
      fSig = f sig
      biTravBody = bitraverseScope f g body
    in
      BFunction name <$> fSig <*> biTravBody

data BTerm b a
  = BVar a
  | BConst !((Int,Int),Double)
  | BZero
  | BPlus (BTerm b a) (BTerm b a)
  | BUnit
  | BPair (BTerm b a) (BTerm b a)
  | BScalar (BTerm b a) (BTerm b a)
  | BDot (BTerm b a) (BTerm b a)
  | BSin (BTerm b a)
  | BCos (BTerm b a)
  | BFst (BTerm b a)
  | BSnd (BTerm b a)
  | BExp (BTerm b a)
  | BFunCall Text  [BTerm b a]
  | BWhen [BBThen b a]
  | BBlock [BStmt b a] (BTerm b a)
  | BLet (BTerm b a) (Scope () (BTerm b) a)
  | BDiffTerm !Int (Scope Int (BTerm b) a) [BTerm b a] [BTerm b a]
  deriving (Functor,Foldable,Traversable)

instance Bifunctor BTerm where
  -- second = fmap
  -- first f t = case t of
  --   BVar c -> BVar c
  --   BConst x0 -> BConst x0
  --   BZero -> BZero
  --   BPlus bt bt' -> BPlus (first f bt) (first f bt')
  --   BUnit -> BUnit
  --   BPair bt bt' -> BPair (first f bt) (first f bt')
  --   BScalar bt bt' -> BScalar (first f bt) (first f bt')
  --   BDot bt bt' -> BDot (first f bt) (first f bt')
  --   BSin bt -> BSin (first f bt)
  --   BCos bt -> BCos (first f bt)
  --   BFst bt -> BFst (first f bt)
  --   BSnd bt -> BSnd (first f bt)
  --   BExp bt -> BExp (first f bt)
  --   BFunCall txt bts -> BFunCall txt (map (first f) bts)
  --   BWhen bts -> BWhen (map (first f) bts)
  --   BBlock bss bt -> BBlock (map (first f) bss) (first f bt)
  --   BLet bt sc -> BLet (first f bt) (hoist (first f) sc)
  --   BDiffTerm nVars m bs vs -> BDiffTerm nVars (hoist (first f) m) (map (first f) bs) (map (first f) vs)
  bimap = bimapDefault   

instance Bifoldable BTerm where
  -- bifoldMap :: (Monoid m) => (a -> m) -> (b -> m) -> (BTerm a b) -> m
  -- bifoldMap f g t = case t of
  --   BVar b -> g b
  --   BConst x0 -> mempty
  --   BZero -> mempty
  --   BPlus bt bt' -> mappend (bifoldMap f g bt) (bifoldMap f g bt')
  --   BUnit -> mempty
  --   BPair bt bt' -> mappend (bifoldMap f g bt)  (bifoldMap f g bt')
  --   BScalar bt bt' -> mappend (bifoldMap f g bt)  (bifoldMap f g bt')
  --   BDot bt bt' -> mappend (bifoldMap f g bt) (bifoldMap f g bt')
  --   BSin bt -> bifoldMap f g bt
  --   BCos bt -> bifoldMap f g bt
  --   BFst bt -> bifoldMap f g bt
  --   BSnd bt -> bifoldMap f g bt
  --   BExp bt -> bifoldMap f g bt
  --   BFunCall txt bts -> foldr (mappend . bifoldMap f g) mempty bts -- foldr/map fusion
  --   BWhen bts -> foldr (mappend . bifoldMap f g) mempty bts
  --   BBlock bss bt -> mappend (foldr (mappend . bifoldMap f g) mempty bss) (bifoldMap f g bt)
  --   BLet bt sc ->
  --     let
  --       bodyUnscoped = fromScope sc
  --       bodyBifolded = bifoldMap f (unvar (const mempty) g) bodyUnscoped
  --     in
  --       mappend (bifoldMap f g bt) bodyBifolded
  --   BDiffTerm n sc bs vs ->
  --     let
  --       ptsVecsCombd = mappend (foldr (mappend . bifoldMap f g) mempty bs) (foldr (mappend . bifoldMap f g) mempty vs)
  --       bodyUnscoped = fromScope sc
  --       bodyBifolded = bifoldMap f (unvar (const mempty) g) bodyUnscoped
  --     in
  --       mappend ptsVecsCombd bodyBifolded
  bifoldMap = bifoldMapDefault   

instance Bitraversable BTerm where
  bitraverse f g t = case t of
    BVar b -> BVar <$> g b
    BConst x0 -> pure (BConst x0)
    BZero -> pure BZero 
    BPlus bt bt' -> BPlus <$> bitraverse f g bt <*> bitraverse f g bt'
    BUnit -> pure BUnit
    BPair bt bt' -> BPair <$> bitraverse f g bt <*> bitraverse f g bt'
    BScalar bt bt' -> BScalar <$> bitraverse f g bt <*> bitraverse f g bt'
    BDot bt bt' -> BDot <$> bitraverse f g bt <*> bitraverse f g bt'
    BSin bt -> BSin <$> bitraverse f g bt
    BCos bt -> BCos <$> bitraverse f g bt
    BFst bt -> BFst <$> bitraverse f g bt
    BSnd bt -> BSnd <$> bitraverse f g bt
    BExp bt -> BExp <$> bitraverse f g bt
    BFunCall txt bts -> BFunCall txt <$> traverse (bitraverse f g) bts
    BWhen bts -> BWhen <$> traverse (bitraverse f g) bts
    BBlock bss bt -> BBlock <$> traverse (bitraverse f g) bss <*> bitraverse f g bt
    BLet bt sc -> 
      let
        btTrav = bitraverse f g bt
        biTravBody = bitraverseScope f g sc
      in
        BLet <$> btTrav <*> biTravBody
    BDiffTerm n sc bs vs -> 
      let
        bsTrav = traverse (bitraverse f g) bs
        vsTrav = traverse (bitraverse f g) vs
        bodyTrav = bitraverseScope f g sc 
      in
        BDiffTerm n <$> bodyTrav <*> bsTrav <*> vsTrav


{-
Substitution into a statement block can be simply stopped.
    {s1; ... sn} [t/x]
    == {x=t; s1; ... sn;}

Thus there can be no variable capture.

From the point of view of type inference.  Nesting scopes and variable shadowing
within a single scope are also unproblematic.  The types introduced into the 
context may overwrite each other as variables are updated.  Upon leaving a
scope, the variable types are reset to what they were before entering the scope.
So again, there is no issue.

Yet another reason to not treat assignments as variable binding is that 
our language treats them as free variables.  The typing rules for 
statement blocks are really flow inferencing -- they just keep 
expanding the context.  That is, there is no variable capture indicated
by the typing rules.  

With the above in mind, we treat (static) assignments as free.  In fact, 
we treat them as nullary function definitions.
-}

instance Applicative (BTerm b) where
  pure = BVar
  (<*>) = ap



instance Monad (BTerm b) where
  return = BVar
  t >>= f = case t of
    BVar a -> f a
    BConst x -> BConst x
    BZero -> BZero
    BPlus bt bt' -> BPlus (bt >>= f) (bt' >>= f)
    BUnit -> BUnit
    BPair bt bt' -> BPair (bt >>= f) (bt' >>= f)
    BScalar bt bt' -> BScalar (bt >>= f) (bt' >>= f)
    BDot bt bt' -> BDot (bt >>= f) (bt' >>= f)
    BSin bt -> BSin (bt >>= f)
    BCos bt -> BCos (bt >>= f)
    BFst bt -> BFst (bt >>= f)
    BSnd bt -> BSnd (bt >>= f)
    BExp bt -> BExp (bt >>= f)
    BFunCall txt bts -> BFunCall txt (map ( >>= f) bts)
    BWhen bts -> BWhen (map (squishAppWhen f) bts)
    BLet m sc -> BLet (m >>= f) (sc >>>= f)
    BBlock ss bt -> BBlock (map (squishAppStmt f) ss) (bt >>= f)
    BDiffTerm n sc pts dirs -> BDiffTerm n (sc >>>= f) (map (>>= f) pts) (map (>>= f) dirs)





-- See https://github.com/ekmett/bound/blob/master/examples/Imperative.hs

{-
Rather than treat blocks as a list of statments followed by a term, we do a more classical 
interpretation through statements and the sequencing operator.  Of course to lower back 
to rust, we linearize the tree of sequences.  We could prune dead stuff, but we don't.
-}
data BStmt b a
  = BLocalDef (BFunction b a)
  | BWhile (BBoolTerm b a) [BStmt b a]
  deriving (Functor,Foldable,Traversable)

instance Bifunctor BStmt where
  -- second = fmap
  -- first f s = case s of
  --   BLocalDef bf -> BLocalDef (first f bf)
  --   BWhile bbt bss -> BWhile (first f bbt) (map (first f) bss)
  bimap = bimapDefault   



squishAppStmt :: (a -> BTerm c b) -> BStmt c a -> BStmt c b
squishAppStmt f t = case t of
  BLocalDef bf -> case bf of
    BFunction txt fnSc sc ->
      let sc' = sc >>>= f
      in BLocalDef $ BFunction txt fnSc sc'
  BWhile bbt bss -> BWhile (squishAppBool f bbt) (map (squishAppStmt f) bss)


instance Bifoldable  BStmt where
  -- bifoldMap f g t = case t of
  --   BLocalDef bf -> bifoldMap f g bf
  --   BWhile bbt bss -> mappend (bifoldMap f g bbt) (foldr (mappend . bifoldMap f g) mempty bss)
  bifoldMap = bifoldMapDefault  

instance Bitraversable BStmt where 
  bitraverse f g stmt = case stmt of 
    BLocalDef bf -> BLocalDef <$> bitraverse f g bf
    BWhile bbt bss -> BWhile <$> bitraverse f g bbt <*> traverse (bitraverse f g) bss


data BBThen b a = BGuard (BBoolTerm b a) (BTerm b a) deriving (Functor,Foldable,Traversable)

instance Bifunctor BBThen where
  -- second = fmap
  -- first f (BGuard b m) = BGuard (first f b) (first f m)
  bimap = bimapDefault   

squishAppWhen :: (a -> BTerm c b) -> BBThen c a -> BBThen c b
squishAppWhen f t = case t of { BGuard bbt bt -> BGuard (squishAppBool f bbt) (bt >>= f) }

instance Bifoldable BBThen where
  -- bifoldMap f g t = case t of
  --   BGuard bbt bt -> mappend (bifoldMap f g bbt) (bifoldMap f g bt)
  bifoldMap = bifoldMapDefault   

instance Bitraversable BBThen where 
  bitraverse f g t = case t of 
    BGuard bbt bt -> BGuard <$> bitraverse f g bbt <*> bitraverse f g bt
    

data BBoolTerm b a
  = BOr (BBoolTerm b a) (BBoolTerm b a)
  | BAnd (BBoolTerm b a) (BBoolTerm b a)
  | BNot (BBoolTerm b a)
  | BLess (BTerm b a) (BTerm b a)
  | BGreater (BTerm b a) (BTerm b a)
  deriving (Functor,Foldable,Traversable)

instance Bifunctor BBoolTerm where
  -- second = fmap
  -- first f b = case b of
  --   BOr bbt bbt' -> BOr (first f bbt) (first f bbt')
  --   BAnd bbt bbt' -> BAnd (first f bbt) (first f bbt')
  --   BNot bbt -> BNot (first f bbt)
  --   BLess bt bt' -> BLess (first f bt) (first f bt')
  --   BGreater bt bt' -> BGreater (first f bt) (first f bt')
  bimap = bimapDefault   



squishAppBool :: (a -> BTerm c b) -> BBoolTerm c a -> BBoolTerm c b
squishAppBool f t = case t of
  BOr bbt bbt' -> BOr (squishAppBool f bbt) (squishAppBool f bbt')
  BAnd bbt bbt' -> BAnd (squishAppBool f bbt) (squishAppBool f bbt')
  BNot bbt -> BNot (squishAppBool f bbt)
  BLess bt bt' -> BLess (bt >>= f) (bt' >>= f)
  BGreater bt bt' -> BGreater (bt >>= f) (bt' >>= f)

instance Bifoldable BBoolTerm where
  -- bifoldMap f g t = case t of
  --   BOr bbt bbt' -> mappend (bifoldMap f g bbt) (bifoldMap f g bbt')
  --   BAnd bbt bbt' -> mappend (bifoldMap f g bbt) (bifoldMap f g bbt')
  --   BNot bbt -> bifoldMap f g bbt
  --   BLess bt bt' -> mappend (bifoldMap f g bt) (bifoldMap f g bt')
  --   BGreater bt bt' -> mappend (bifoldMap f g bt) (bifoldMap f g bt')
  bifoldMap = bifoldMapDefault   

instance Bitraversable BBoolTerm where 
  bitraverse f g t = case t of 
    BOr bbt bbt' -> BOr <$> bitraverse f g bbt <*> bitraverse f g bbt'
    BAnd bbt bbt' -> BAnd <$> bitraverse f g bbt <*> bitraverse f g bbt'
    BNot bbt -> BNot <$> bitraverse f g bbt
    BLess bt bt' -> BLess <$> bitraverse f g bt <*> bitraverse f g bt'
    BGreater bt bt' -> BGreater <$> bitraverse f g bt <*> bitraverse f g bt'
