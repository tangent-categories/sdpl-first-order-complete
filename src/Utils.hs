module Utils where 
    
import qualified Data.List

listToMap :: Eq a => [a] -> a -> Maybe Int 
listToMap = flip Data.List.elemIndex