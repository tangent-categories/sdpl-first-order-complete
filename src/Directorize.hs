{- |
This module converts terms to director string form.  

The main idea behind a director string is to keep track of how variables/information flow through a term.
In the lambda calculus this can be used to bypass expensive reductions that are going to be eliminated anyways.
For our purposes, we are using them for bypassing the expensive evaluation of a term that is going to be differentiated to zero 
namely differentiation of a term with respect to a variable that does not exist.  While a programmer may not write 
such a term -- they are generated as a biproduct of differentiating loops and recursive calls in some cases.

After directorization, the free variables of a term are computed in constant time by simply returning the 
director set at that node.
-}
module Directorize where

import Data.Set (Set)
import qualified Data.Set as S
import Data.Text (Text)
import Language.Abs
import TermUtils
import Data.Bifunctor (first,second,bimap)

{- |
Input: a program
Output: the directorized program and the set of free variables in that program.
Note this makes sense because a program could reference global constants or possibly constants from a different module.
Note! We are flexible with the treatment of top level symbols.  We should make sure that once we finalize the top level semantics
that this module agrees with everything.  In particular, we currently remove any declared constants from the set of free variables 
of a program.  The idea is that if x occurs in m, then x = m as a top level entity is treated as a recursively defined constant.
There are times in numerical programming where recursively defined constants can be useful, for example in machine epsilon testing.
-}
directProg :: Prog' a -> (Prog' (Set Text),Set Text)
directProg (Program _ decls) =
    let directedDeclsAndFree = map directDecl decls in
    let (directedDecls,declFrees) = unzip directedDeclsAndFree in
    let freeSet = S.unions declFrees in
    let constants = [pIdentToText constName | ConstBinding _ constName _ <- decls] in
    let freeVars = freeSet S.\\ S.fromList constants in
    (Program freeVars directedDecls,freeVars)

freeVarsDirectedProg :: Prog' (Set Text) -> Set Text
freeVarsDirectedProg (Program frees _) = frees

isFreeProg :: Text -> Prog' (Set Text) -> Bool
isFreeProg name p = S.member name (freeVarsDirectedProg p)


directDecl :: Decl' a -> (Decl' (Set Text),Set Text)
directDecl d = case d of
  -- in `x = m` x may occur freely (even possibly with a different type, which is shadowed by the new use of the name x) 
  ConstBinding _ p te -> let (dte,freete) = directTerm te in (ConstBinding freete p dte,freete)
  FunBinding _ pi pis te ->
    let (dte,freete) = directTerm te in
    let bounded = S.fromList (map pIdentToText pis) in
    let freesMinusBounded = freete S.\\ bounded in
    (FunBinding freesMinusBounded pi pis dte,freesMinusBounded)

freeVarsDirectedDecl :: Decl' (Set Text) -> Set Text
freeVarsDirectedDecl d = case d of
  ConstBinding set _ _ -> set
  FunBinding set _ _ _ -> set

isFreeDecl :: Text -> Decl' (Set Text) -> Bool
isFreeDecl name d = S.member name (freeVarsDirectedDecl d)

directTerm :: Term' a -> (Term' (Set Text), S.Set Text)
directTerm t = case t of
  Plus a te te' ->
    let (te1,free_te) = directTerm te in
    let (te2,free_te') = directTerm te' in
    let frees = free_te `S.union` free_te' in
    (Plus frees te1 te2,frees)
  Zero _ -> (Zero S.empty,S.empty)
  Scalar _ te te' ->
    let
        (te1,free_te1) = directTerm te
        (te2,free_te2) = directTerm te'
        frees = free_te1 `S.union` free_te2
    in
        (Scalar frees te1 te2,frees)
  Dot _ te te' ->
    let
        (te1,free_te1) = directTerm te
        (te2,free_te2) = directTerm te'
        frees = free_te1 `S.union` free_te2
    in
        (Dot frees te1 te2,frees)
  Unit _ -> (Unit S.empty,S.empty)
  Pair _ te te' ->
    let
        (te1,free_te1) = directTerm te
        (te2,free_te2) = directTerm te'
        frees = free_te1 `S.union` free_te2
    in
        (Pair frees te1 te2,frees)
  Sin _ te ->
    let (te1,free_te1) = directTerm te
    in (Sin free_te1 te1,free_te1)
  Cos _ te ->
    let (te1,free_te1) = directTerm te
    in (Cos free_te1 te1,free_te1)
  Exp _ te ->
    let (te1,free_te1) = directTerm te
    in (Exp free_te1 te1,free_te1)
  Fst _ te ->
    let (te1,free_te1) = directTerm te
    in (Fst free_te1 te1,free_te1)
  Snd _ te ->
    let (te1,free_te1) = directTerm te
    in (Snd free_te1 te1,free_te1)
  Var _ pi -> let frees = S.singleton (pIdentToText pi) in (Var frees pi,frees)
  FunCall _ pi tes ->
    let
        dtesfree = map directTerm tes
        (dtes,freeList) = unzip dtesfree
        frees = S.unions freeList
    in
        (FunCall frees pi dtes,frees)
  Const _ num -> (Const S.empty num,S.empty)
  When _ bts ->
    let
        btsfree = map directBThen bts
        (btsD,freeList) = unzip btsfree
        frees = S.unions freeList
    in
        (When frees btsD,frees)
  -- let x = m in n
  Let _ x m n ->
    let
        (mD,freeM) = directTerm m
        (nD,freeNRaw) = directTerm n
        frees = freeM `S.union` (freeNRaw S.\\ S.singleton (pIdentToText x))
    in
        (Let frees x mD nD,frees)
  BlockTerm _ sts te -> 
    let 
        (stmtsD,binding,freesStmts) = directBlock sts S.empty
        (teD,freeTe) = directTerm te 
        frees = freesStmts `S.union` (freeTe S.\\ binding)
    in 
        (BlockTerm frees stmtsD teD,frees)
  DiffTerm _ m pis bs vs -> 
    let 
        bounded = S.fromList $ map pIdentToText pis 
        (mD,freeMRaw) = directTerm m 
        (bsD,freesBs) = second S.unions $ unzip $ map directTerm bs 
        (vsD,freesVs) = second S.unions $ unzip $ map directTerm vs
        frees = (freeMRaw S.\\ bounded) `S.union` freesBs `S.union` freesVs
    in 
        (DiffTerm frees mD pis bsD vsD,frees)
    


{- |
Inputs:
  stmts: a list of statements that bind in m
  m: the ultimate term in the block
  bounded: the list of bindings found so far
Output: 
  a list of statements converted to director string form
  the binding variables collected by the block
  the set of free variables in the final binding term

TESTING NEEDED: 
  make sure we correctly compute the free variables at each node for 
  f(x) = 
    do 
        y = x
        x=5
    in 
        x
Here x is free in the do block total but bound in the last term.
-}
directBlock :: [Stmt' a] -> Set Text -> ([Stmt' (Set Text)], Set Text, Set Text)
directBlock stmts bounded = case stmts of 
  [] -> ([],bounded,S.empty)
  st : sts -> case st of
    (DeclStmt _ de) -> case de of
      (ConstBinding _ pi _) -> 
          let
              -- we have a top level statement de of the form pi = m with bounded bound variables.
              -- so we get the directorized version of de but then we have to remove bounded from it's free variables
              -- and remove the identifier pi from the rest of the block.
            (deD,freesDeRaw) = directDecl de
            freesDe = freesDeRaw S.\\ bounded 
            (stmtsD,newBounded,frees) = directBlock stmts (S.insert (pIdentToText pi) bounded)
          in
            (DeclStmt freesDe deD : stmtsD , newBounded, frees `S.union` freesDe)

      _ -> 
        let
          (deD,freesDe) = directDecl de
          (stmtsD,newBounded,frees) = directBlock stmts bounded  
        in
          (DeclStmt freesDe deD : stmtsD, newBounded, frees `S.union` freesDe)
    (While _ bt whileBlock) -> 
      let
        (btD,freesBt) = directBool bt
        (whileBlockD,_,freesWhile) = directBlock whileBlock S.empty
        -- a while loop cannot exit with new variables created
        freesInWhileStmt = freesWhile `S.union` freesBt
        (stmtsD,newBounded,frees) = directBlock stmts bounded 

      in 
        (While freesInWhileStmt btD whileBlockD : stmtsD,newBounded,frees `S.union` freesInWhileStmt)


directBThen :: BThen' a -> (BThen' (Set Text),Set Text)
directBThen (Guard _ bt te) =
    let 
        (btD,freesBt) = directBool bt 
        (teD,freesTe) = directTerm te
        frees = freesBt `S.union` freesTe
    in 
        (Guard frees btD teD,frees )

directBool :: BoolTerm' a -> (BoolTerm' (Set Text), Set Text)
directBool t = case t of 
  Or _ bt bt' -> 
    let 
        (bt1,freesBt1) = directBool bt 
        (bt2,freesBt2) = directBool bt' 
        frees = freesBt1 `S.union` freesBt2
    in 
        (Or frees bt1 bt2, frees)
  And a bt bt' -> 
    let 
        (bt1,freesBt1) = directBool bt 
        (bt2,freesBt2) = directBool bt' 
        frees = freesBt1 `S.union` freesBt2
    in 
        (And frees bt1 bt2, frees)
  Not _ bt -> let (btD,frees) = directBool bt in (Not frees btD,frees)
  Less a te te' -> 
    let
        (te1,free_te1) = directTerm te
        (te2,free_te2) = directTerm te'
        frees = free_te1 `S.union` free_te2
    in
        (Less frees te1 te2,frees)
  Greater a te te' -> 
    let
        (te1,free_te1) = directTerm te
        (te2,free_te2) = directTerm te'
        frees = free_te1 `S.union` free_te2
    in
        (Greater frees te1 te2,frees)

    




