{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE FlexibleContexts  #-}
module DPLTypes where

import Bound
import Control.Unification
import Control.Unification.IntVar
import Control.Unification.Types
import Control.Monad.Identity
import Control.Monad.Trans
import Control.Monad.State.Strict
import Control.Monad.Except

import qualified Bound.Term as BT

import qualified Data.Map as M

import Data.Text(Text)
import qualified Data.Text as Text
import Stream
import qualified Data.List
{-
IGNORE NOTE:
You may safely ignore everything from this point to the next "IGNORE NOTE" point.
This contains the code required to bootstrap the Unification-fd library for our language of types.

However, it may be helpful to know a little about what's happening behind the sences.  Since our language is polymorphic, it may contain 
type variables.  The language of types may be specified 
by a data type as follows:
    data Type a = 
        TR
        | Unit 
        | Pair (Type a) (Type a)
        | TypeVar a

However, in order to perform syntactic unification of types as terms, they are required to be an "absolutely free" or "anarchic" algebra aka a "term algebra"  In Haskell, this is enforced 
by requiring the language of types that we perform unification on to be constructed as a free monad.  Note the shape of the constructors of Type a.  There is a single constructor TypeVar a
with a non-recursive use of a, and the other uses of a are only recursive within TPair.  This means that if we define 

    data TypeFormal b = 
        R 
        | TUnit 
        | TPair b b 

Then we can almost recover type by setting Type a = TypeFormal (TypeFormal a); however, we'd be missing the TypeVar.  The constructor UTerm does this and defines a Var constructor for us.  Formally, UTerm is defined by 
    data UTerm f a =
        UTerm (f (UTerm f a))
        | UVar a

If we then consider UTerm TypeFormal a we wee 
    data UTerm TypeFormal a 
    == 
        UTerm R 
        | UTerm TUnit 
        | UTerm (TPair (UTerm TypeFormal a) (UTerm TypeFormal a))
        | UVar a
Which we can clearly convert back and forth to the original Type without loss. 
-}


data TypeFormal a =
    R
    | TUnit
    | TPair a a
    deriving (Show,Eq,Functor,Foldable,Traversable)

type Type = UTerm TypeFormal IntVar
type IntBinding = IntBindingT TypeFormal Identity
type TypingFailure = UFailure TypeFormal IntVar

{-
When performing type inference, we record types of variables and functions as type schemes.  Variables that come from function inputs and let bindings 
will have all their type variables abstracted in the type scheme, and variables that come from differentiation will have their variables completely open.
Note that the tutorial language lacks let-bindings, function declarations, and derivatives, so the use of a type scheme is overkill -- however, it will be 
useful in the real language.  Also, the use of the FunTypeEnv here is un-needed since the tutorial language has no function declarations.  However, again, 
the setup here should be usable in the real language.

A type scheme is a way to specify a generic type while binding the type variables so that only the shape of the type matters.  For example, we write 
    Forall a,b. (a,(a,b))
To indicate a tuple whose second element is a tuple.  By binding the variables, the above type is exactly the same as 
    Forall c,r. (c,(c,r))
Note, this is done implicitly in some languages.  For example, in Haskell, one would simply write 
    (a,(a,b))
And the Forall a,b is silently added to the front of the type scheme.  This works because the quantifiers all appear at the outermost level (though Haskell does support quantifiers at deeper levels -- these have to then be explicitly marked).

Since we are dealing with type schemes, which have bound variables, we use the Bound library to keep track of the bound variables.  Without getting into details, the bound library allows 
us to specify that we are treating 
    Forall a,b. (a,(a,b))
as a bound type.  Then, all wee need to record is the number of variables, and then we use the scope bit to allow us to make a sort of type-level function that can be evaluated to 
obtain an actual type.  In the bound library, Forall a,b. (a,(a,b)) is essentially represented as 
    Forall 2 (\x y.(x,(x,y)))
In fact we can take the type (x,(x,y)) and abstract out its variables to obtain the scope -- that is we can get the internal representation as 
    Forall 2 (abstract x y ((x,(x,y))))
And then we can instantiate this with actual types, a,b with 
    instantiate [a,b] (\x y.(x,(x,y))) 
To obtain the concrete type 
    (a,(a,b))
The above description isn't exactly accurate, as we've oversimplified a few details of how the library works in practice.
However, programmatically, it works out pretty simply.  Ask if you have any questions about how to use the library.  You can also find a more full-featured 
    implementation in the simple-functional-lang project on this gitlab repository.
-}
-- data Scheme a = Forall Int (Scope Int (UTerm TypeFormal) a)
data FunScheme a = Forall Int ([Scope Int (UTerm TypeFormal) a],Scope Int (UTerm TypeFormal) a)

instance Show a => Show (FunScheme a) where
    show t = prettifyScheme t

{-
Helper function: lift a scope to a value function scheme -- that is a function scheme with no arguments.
PRE-condition: the scope sc must be bound with n in order Forall n sc to have the right semantics -- that is 
  the number used in making the scope must be 
-}


-- type VarTypeEnv a = M.Map Text (Scheme a)
-- type FunTypeEnv a = M.Map Text (Scheme ([a],a))
type TypeEnv a = M.Map Text (FunScheme a)

-- Utilities for constructing types as Type
pair :: UTerm TypeFormal v -> UTerm TypeFormal v -> UTerm TypeFormal v
pair m n = UTerm $ TPair m n
unit :: UTerm TypeFormal v
unit = UTerm TUnit
real :: UTerm TypeFormal v
real = UTerm R


{-
This instance declaration provides all the real work in the unification.  All we need to do is describe how 
to match TypeFormal (the non-recursive datatype), and the matching of arbitrary UTerms is handled for us, along 
with the unification.-- Convenience function for getting a fresh variable in the binding monad
getFreshVar :: (MonadTrans t1,BindingMonad t2 v m) => t1 m (UTerm t2 v)
getFreshVar = lift $ UVar <$> freeVar

-}
instance Unifiable TypeFormal where
    zipMatch R R = Just R
    zipMatch TUnit TUnit = Just TUnit
    zipMatch (TPair l1 r1) (TPair l2 r2) = Just $ TPair (Right (l1,l2)) (Right (r1,r2))
    zipMatch _ _ = Nothing

-- precondition numVars is the number of variables bound in the types
-- Note if numVars is 0 (or <= 0) then take numVars varNames has length 0.
--   Then take numVars varNames !! is the everywhere undefined function.
--   But then instantiate reduces to projecting on all the free variables.
fullInstantiateFunSchemeWith :: FunScheme a -> [a] -> ([UTerm TypeFormal a],UTerm TypeFormal a)
fullInstantiateFunSchemeWith (Forall numVars (scs,sc)) varNames =
    (map (instantiate instr) scs, instantiate instr sc) where instr = UVar . ( take numVars varNames !!)


fullInstantiateFunSchemeWithFresh :: (MonadTrans t1, BindingMonad TypeFormal a m, Monad (t1 m))
    => FunScheme a
    -> t1 m ([UTerm TypeFormal a],UTerm TypeFormal a)
fullInstantiateFunSchemeWithFresh (Forall numVars (scs,sc)) = do
    streamedFresh <- streamFreshVars numVars
    let instr = (streamedFresh !!)
    return (map (instantiate instr) scs, instantiate instr sc)


{-
Warning!  If you try to generlize the following to return an infinite stream of type 
variables and then "take" them as needed you will get a runtime error.  As the saying 
goes, there's lazy and then there's **lazy**  Haskell is merely lazy.  Indeed, the various 
monads involved will force, through the use of bind, full evaluation of [1..] and the 
computation will diverge.
-}
streamFreshVars :: (MonadTrans t1,BindingMonad t2 v m,Monad (t1 m)) => Int -> t1 m [UTerm t2 v]
streamFreshVars n = mapM (const getFreshVar) [1 ..n]


-- Convenience function for getting a fresh variable in the binding monad
getFreshVar :: (MonadTrans t1,BindingMonad t2 v m) => t1 m (UTerm t2 v)
getFreshVar = lift $ UVar <$> freeVar

----------------- pretty printing stuff goes here 
-- The assumption on a scheme is that it's closed.
-- thus we can change how's bound.
prettifyScheme :: Show a => FunScheme a -> String
prettifyScheme sc = case sc of
    Forall n (iSchs,oSch) ->
        let (varSupply,_) = grab n globalVarSupplyString in
        case (mapM BT.closed iSchs,BT.closed oSch) of -- scStringy :: FunScheme String 
            (Just iSchs',Just oSch') ->
                let (prettyInputs,prettyOutput) = (map (prettyStringTypeScope varSupply) iSchs',prettyStringTypeScope varSupply oSch') in
                "∀ " ++ Data.List.intercalate "," varSupply ++ "." ++ Data.List.intercalate ", " prettyInputs ++ " -> " ++ prettyOutput
            _ -> "naaaah, not a real scheme"


prettyStringTypeScope :: [String] -> Scope Int (UTerm TypeFormal) String -> String
prettyStringTypeScope varNames sc = prettyStringType $ instantiate (UVar . (varNames !!)) sc

prettyStringType :: UTerm TypeFormal String -> String
prettyStringType t = case t of 
  UVar s -> s
  UTerm tf -> case tf of 
    R -> "R"
    TUnit -> "()"
    TPair ut ut' -> "(" ++ prettyStringType ut ++ "," ++ prettyStringType ut' ++")"





