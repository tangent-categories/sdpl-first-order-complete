module BoundCoreToSubRust where

import Bound

import BoundCore 
import SubRust.Abs
import qualified Language.Abs as RawLanguage (PIdent (..),Number (..))
import DPLTypes

import Control.Unification

import Data.Text (Text)
import qualified Data.Text as Text

import Control.Monad.State.Strict

import Stream

{-
In the sublanguage of Rust we work in inputs and outputs are all explicitly 
typed, if generic.  We perform type inference, prettify the type, 
and hand the whole beautiful thing to rust. 

By Algorithm W, our functions are given type schemes:
    Forall a,... . Type
where Type is closed in the variables bound by Forall
Similarly, generic rust type signatures are literally type schemes
    fn <A,...> (x1:A1,..,xm:Am) -> B {body}
where all the Ai and B are closed in the variables bound in the <A,...>
In other words, we perform our type checking here.

The below program makes it seem that programs must be 1 shot type checked.
However, one can simply load up the environment with previously known 
type schemes for symbol names and easily extend this for type checking over imported 
modules.  However, to get something more like ML-style modules would possibly 
be a bit more work.

When we perform type inference, of say a BTerm a, then we create a BTerm (a,Type), 
so that all variables hold their types along with them.  Then 
the subsequent differentiation engine will work with these types to give a 
CTerm (a,Type).  Which we can then assume to inherit here.  Thus 
we lower on programs where variables are stored with their types.
This allows us to proceed without needing the symbol table.
-}
coreToRustProg :: CProg (FunScheme Text) Text -> Prog' ()
coreToRustProg t = case t of 
    CProg cfs -> Program () (map coreToRustFunction cfs) --Program () (map coreToRustFunction cfs)

coreToRustFunction ::  CFunction (FunScheme Text) Text -> Decl' ()
coreToRustFunction (CFunction (r, c, name) fnSc@(Forall n x0) body) =
    let (tyVarNames,_) = grab n rustTypeVarSupply  in -- get the abstract type variable names, if any
    let (argTys,outTy) = fullInstantiateFunSchemeWith fnSc tyVarNames in
    let (varNames,varSupply) = grab n globalVarSupply in
    let realBody = instantiate (\n -> CVar (varNames !! n)) body in 
    -- let realBody = evalState realBodyS varSupply in
    let rDecl = makeDecl n tyVarNames varNames argTys in
    let rOutTy = makeOutTy n outTy tyVarNames in
    let rustBody = evalState (coreToRustTerm realBody) varSupply  in 
    case outTy of
      UTerm TUnit -> VoidFunBinding () (localTextToRustPIdent ((r,c),name)) rDecl (map makeTraitedTypeVar tyVarNames) rustBody
      _ -> FunBinding () (localTextToRustPIdent ((r,c),name)) rDecl rOutTy rustBody

        
makeOutTy :: Int -> UTerm TypeFormal Text -> [Text] -> OutputDecl' ()
makeOutTy numVars outTy tyVarNames = OutputTraits () (dplTypeToRustType outTy) (map makeTraitedTypeVar tyVarNames)

makeTraitedTypeVar :: Text -> TraitedTypeVar' ()
makeTraitedTypeVar tyVar = DplTrait () (textToRustPIdentLost tyVar)

makeDecl :: Int -> [Text] -> [Text] -> [UTerm TypeFormal Text] -> InputDecl' ()
makeDecl numVars tyVarNames varNames varTys = if numVars <= 0 
    then Concrete () (zipWith makeTypedVar varNames varTys)
    else Generic () (map textToRustPIdentLost tyVarNames) (zipWith makeTypedVar varNames varTys)

makeTypedVar :: Text -> UTerm TypeFormal Text -> ArgType' ()
makeTypedVar varName varType = TypedVariable () (textToRustPIdentLost varName) (dplTypeToRustType varType)

dplTypeToRustType :: UTerm TypeFormal Text -> RType' ()
dplTypeToRustType t = case t of 
  UVar txt -> RTVar () (textToRustPIdentLost txt)
  UTerm tf -> case tf of
    R -> RF64 ()
    TUnit -> RUnit ()
    (TPair ut ut') -> RPair () (dplTypeToRustType ut) (dplTypeToRustType ut')


{-
Rewrite this function to run in the state monad!!!
-}
coreToRustTerm ::  CTerm (FunScheme Text) Text -> State (Stream Text) (Term' ())
coreToRustTerm  t  = case t of 
  CVar txt -> return $ RVar () (textToRustPIdentLost txt) 
  CConst x0 -> return $ RConst () (localDoubleToRustNum x0)
  CZero -> return $ RZero ()
  CPlus ct ct' -> do 
      ct1 <- coreToRustTerm ct
      ct2 <- coreToRustTerm ct'
      return $ RPlus () ct1 ct2
  CUnit -> return $ RUTerm ()
  CPair ct ct' -> do 
      ct1 <- coreToRustTerm ct
      ct2 <- coreToRustTerm ct'
      return $ RPairTerm () ct1 ct2
  CScalar ct ct' ->do 
      ct1 <- coreToRustTerm ct
      ct2 <- coreToRustTerm ct'
      return $ RScalar () ct1 ct2
  CDot ct ct' -> do 
      ct1 <- coreToRustTerm ct
      ct2 <- coreToRustTerm ct'
      return $ RDot () ct1 ct2
  CSin ct -> do 
      ct' <- coreToRustTerm ct 
      return $ RSin () ct'
  CCos ct ->do 
      ct' <- coreToRustTerm ct 
      return $ RCos () ct'
  CFst ct -> do 
      ct' <- coreToRustTerm ct 
      return $ RFst () ct'
  CSnd ct -> do 
      ct' <- coreToRustTerm ct 
      return $ RSnd () ct'
  CExp ct -> do 
      ct' <- coreToRustTerm ct 
      return $ RExp () ct'
  CFunCall txt cts -> do 
      cts' <- mapM coreToRustTerm cts 
      return $ RCall () (textToRustPIdentLost txt) cts'
  CWhen cts -> do 
      cts' <- mapM coreToRustGuard cts 
      return $ RGMatch () cts'
  CBlock css ct -> do 
      css' <- mapM coreToRustStmt css 
      ct' <- coreToRustTerm ct 
      return $ BlockTerm () css' ct'
  CLet ct sc -> do 
    termToBeSubbed <- coreToRustTerm ct 
    (varName,_) <- popM
    let termToSubInto = instantiate1 (CVar varName) sc 
    termToSubIntoR <- coreToRustTerm termToSubInto
    let asst = AsstStmt () (textToRustPIdentLost varName) termToBeSubbed
    return $ BlockTerm () [asst] termToSubIntoR

coreToRustGuard :: CBThen (FunScheme Text) Text -> State (Stream Text)  (MatchStmt' ())
coreToRustGuard t = case t of 
    CGuard cbt ct -> do 
        cbt' <- coreToRustBool cbt 
        ct' <- coreToRustTerm ct 
        return $ PureGuard () cbt' ct'
    

coreToRustStmt :: CStmt (FunScheme Text) Text -> State (Stream Text) (Stmt' ())
coreToRustStmt t = case t of 
  CLocalDef cf -> case cf of 
    CFunction (r,c,name) fs@(Forall n x1) sc -> if n <= 0 
        then do 
            -- in this case the function abstracts no variables, hence, 
            -- sc has no bound variables, and we can just project 
            let cTerm = instantiate (const CUnit ) sc
            rTerm <- coreToRustTerm cTerm
            return $ AsstStmt () (localTextToRustPIdent ((r,c),name)) rTerm
        else do 
            -- in this case the function is abstracting variables
            (argNames,_) <- grabM n
            let realBody = instantiate (\n -> CVar (argNames !! n)) sc
            rBody <- coreToRustTerm realBody
            return $ ClosureStmt () (localTextToRustPIdent ((r,c),name)) (map textToRustPIdentLost argNames) rBody

  CWhile cbt css -> do 
      cbt' <- coreToRustBool cbt 
      css' <- mapM coreToRustStmt css 
      return $ WhileStmt () cbt' css' 

coreToRustBool :: CBoolTerm (FunScheme Text) Text -> State (Stream Text)  (BTerm' ())
coreToRustBool t = case t of 
  COr cbt cbt' -> do 
      cbt1 <- coreToRustBool cbt
      cbt2 <- coreToRustBool cbt' 
      return $ ROr () cbt1 cbt2 
  CAnd cbt cbt' -> do 
      cbt1 <- coreToRustBool cbt 
      cbt2 <- coreToRustBool cbt' 
      return $ RAnd () cbt1 cbt2
  CNot cbt -> do 
      cbt1 <- coreToRustBool cbt 
      return $ RNot () cbt1
  CLess ct ct' -> do 
      ct1 <- coreToRustTerm ct 
      ct2 <- coreToRustTerm ct' 
      return $ RLess () ct1 ct2 
  CGreater ct ct' -> do 
      ct1 <- coreToRustTerm ct 
      ct2 <- coreToRustTerm ct'
      return $ RGreater () ct1 ct2

        -- instantiate (\n -> CVar (varNames !! n)) body


popM :: State (Stream a)  (a, Stream a)
popM = do 
    (h,t) <- gets pop 
    put t
    return (h,t)

grabM :: Int -> State (Stream a) ([a],Stream a)
grabM n = do 
    (initial,leftover) <- gets (grab n)
    put leftover 
    return (initial,leftover)

-- type RType = RType' BNFC'Position
-- data RType' a
--     = RUnit a | RF64 a | RTVar a PIdent | RPair a (RType' a) (RType' a)
--   deriving (C.Eq, C.Ord, C.Show, C.Read, C.Functor, C.Foldable, C.Traversable)

    --let in -- get a concrete function type for the function
    -- By type checking, we may guarantee that sc is scoped on length bs.
    -- We can then check the type of b and see if we're doing a void thing.
    -- CFunction (r,c,name) bs (UTerm TUnit) sc 
    --     -> VoidFunBinding 
    --         ()                        -- we're not shoving BNFC'Position everywhere
    --         (PIdent ((r,c),name))     -- the location aware name of the function
    --         undefined                 -- input declarations
    --         undefined                 -- statements
    --         undefined                 -- return argument
    -- CFunction (r,c,name) bs b sc = error "not implemented"-> FunBinding () (PIdent ((r,c),name)) undefined undefined undefined undefined
    

-- type InputDecl = InputDecl' BNFC'Position
-- data InputDecl' a
--     = Concrete a [ArgType' a] | Generic a [RType' a] [ArgType' a]
--   deriving (C.Eq, C.Ord, C.Show, C.Read, C.Functor, C.Foldable, C.Traversable)


-- Positional reconstruction
langToRustPIdent :: RawLanguage.PIdent -> PIdent 
langToRustPIdent t = case t of
    RawLanguage.PIdent ((r,c),t) -> PIdent ((r,c),t)

langToRustNum :: RawLanguage.Number -> Number 
langToRustNum (RawLanguage.Number ((r,c),t)) = Number ((r,c),t)

textToRustPIdentLost :: Text -> PIdent 
textToRustPIdentLost t = PIdent ((-1,-1),t) -- clearly this t doesn't know where to go

doubleToRustNumLost :: Double -> Number 
doubleToRustNumLost d =Number ((-1,-1),Text.pack (show d))

localDoubleToRustNum :: ((Int,Int),Double) -> Number 
localDoubleToRustNum ((r,c),d) = Number ((r,c),Text.pack (show d))

localTextToRustPIdent :: ((Int, Int), Text) -> PIdent
localTextToRustPIdent = PIdent