module Stream where 

import Control.Monad

import Data.Text(Text)
import qualified Data.Text as Text

{-
Nats are initial for 1 -> a and a -> a,  that is given 
  z :: a 
  s :: a -> a 
There is a unique 
  fold(z,s) :: Nats -> a 
And moreover 
   fold(z,s) 0 = z
   fold(z,s) (n+1) = s (fold (z,s) n)

Likewise streams are final for a -> b and b -> b 
That is, given 
  h :: a -> b 
  t :: a -> a
There is a unique 
  unfold(h,t) :: a -> Stream b 
And moreover 
  head (unfold(h,t) z) = h z 
  and 
  tail (unfold (h,t) z) = unfold (h,t) (t z)
-}
data Stream a = Stream {headS :: !a, tailS :: Stream a} deriving (Eq,Ord)

instance Show a => Show (Stream a) where 
    show s = "The stream has head: " ++ show (headS s)

instance Functor Stream where 
    fmap f s = Stream {headS = f (headS s), tailS = fmap f (tailS s)}



unfold :: (a -> b) -> (a -> a) -> a -> Stream b 
unfold h t z = Stream {headS = h z, tailS = unfold h t (t z)}

{-
Then for example the stream of natural numbers is 
-}
data Nat = Zero | Succ Nat

foldNat :: a -> (a -> a) -> Nat -> a 
foldNat z s Zero = z 
foldNat z s (Succ n) = s (foldNat z s n)

infixr 5 .>
(.>) :: (a -> b) -> (b -> c) -> (a -> c)
f .> g = g . f

infixr 5 .>> 
(.>>) :: a -> (a -> b) -> b 
x .>> f = f x


type StreamN a = Nat -> a
unfold2 :: (a -> b) -> (a -> a) -> a -> StreamN b 
unfold2 h t z = foldNat z t .> h

-- One can construct the identity function using unfold.
pleonasticId :: Stream a -> Stream a
{-
headS :: Stream a -> a
tailS :: Stream a -> Stream a
unfold headS tailS :: Stream a -> Stream a
-}
pleonasticId = unfold headS tailS

{-
Now, we can give heads and tails for StreamN streams too.
-}
headN :: StreamN a -> a 
headN s = s Zero 

-- for tail we shift everything by +1.  Since the stream is a Nat -> a and we have a (+1) :: Nat -> Nat we can just compose
tailN :: StreamN a -> StreamN a
tailN = (Succ .>)

{-
Now we can translate between the stream types by just unfolding with the other 
destructors
-}
translateNtoStream :: StreamN a -> Stream a
translateNtoStream = unfold headN tailN

translateStreamToN :: Stream a -> StreamN a 
translateStreamToN = unfold2 headS tailS

{--
Now, since we're in a cartesian category, every type is a comonoid, so every representable
functor is a monad.  In particular StreamN a is a monad.  To see this, consider the join.
-}
joinN :: StreamN (StreamN a) -> StreamN a
joinN f z = f z z

{--
There is also a nice pure definition
-}
pureN :: a -> StreamN a -- a -> Nat -> a
pureN = const

{-
StreamN is also a (representable) functor.
Here fmap f == Nat -> f
So here f :: a -> b and we're given g :: StreamN a == Nat -> a.
So we compose g .> f :: Nat -> b as required.
-}
fmapN :: (a -> b) -> StreamN a -> StreamN b
fmapN f g = g .> f

{--
Note the monad laws:
  for x :: StreamN a, we have:

  joinN (pureN x)
  == \z -> (pureN x) z z 
  == \z -> (const x) z z 
  == \z -> const x z z
  == \z -> x z 
  == x -- by eta equality 

  joinN ((StreamN pureN) x)
  == joinN ((Nat -> pureN) x)
  ... Recall that for a type A and a function f: B \to C 
  ... the map A -> f :: (A -> B) -> (A -> C)
  ... is defined as (A -> f) g = g .> f :: A -> C
  == joinN (x .> pureN)
  == joinN (x .> const)
  == \z -> (x .> const) z z
  == \z -> (const . x) z z
  == \z -> (\t -> const (x t)) z z
  == \z -> const (x z) z
  == \z -> x z
  == x -- by eta equality

  Hence StreamN is a monad.  If we define the monad instance of Stream 
  through the isomorphism then Stream is a monad too.
-}
instance Applicative Stream where 
    pure = pureN .> translateNtoStream
    (<*>) = ap 

instance Monad Stream where 
    return = pureN .> translateNtoStream
    s >>= f = s .>> translateStreamToN .> fmapN (f .> translateStreamToN) .> joinN .> translateNtoStream


constantStream :: a -> Stream a 
constantStream = return 

--unfold :: (a -> b) -> (a -> a) -> a -> Stream b 

{-
Given two streams (s1,s2) of type Stream a

  (Stream a,Stream a) -> a 
  (Stream a,Stream a) -> (Stream a, Stream a) 
  (Stream a, Stream a)
  ----------------------------------------------
  Stream a

  We take the head of the first stream then we swap the streams
  Then we take the head of the first stream, which was originally the second
  And so on, creating an interleaving of the streams
-}
interleaveStream :: Stream b -> Stream b -> Stream b
interleaveStream s1 s2 = unfold (fst .> headS) (\(a,b) -> (b,a)) (s1,s2)

-- How to make a ring in haskell 
-- ringIfy :: [a] -> [a]
-- ringIfy zs = ringHelp zs zs 
--     where 
--         ringHelp [] zs = ringHelp zs zs 
--         ringHelp (x:xs) zs = x : ringHelp xs zs

ringIfy :: [a] -> Stream a
ringIfy zs = ringHelp zs zs 
    where 
        ringHelp [] zs = ringHelp zs zs 
        ringHelp (x:xs) zs = Stream {headS = x, tailS = ringHelp xs zs}

-- ringIfyGrow :: [a] -> (a -> b) -> Stream b 
ringIfyGrow zs z g f = ringHelp zs zs z g f 
    where 
        ringHelp [] zs z  g f =  ringHelp zs zs (g z) g f 
        ringHelp (x:xs) zs z g f = Stream {headS = f x z, tailS = ringHelp xs zs z g f}

-- aToZ :: [String]
-- aToZ = map return ['a' ..'z']

aToZ :: [Char]
aToZ = ['a' ..'z']

globalVarSupplyString :: Stream String
globalVarSupplyString = ringIfyGrow aToZ "" ('0':) (:)

globalVarSupply :: Stream Text 
globalVarSupply = fmap Text.pack globalVarSupplyString

rustTypeVarSupplyS :: Stream String
rustTypeVarSupplyS = ringIfyGrow ['A' ..'Z'] "" ('0':) (:)

rustTypeVarSupply :: Stream Text 
rustTypeVarSupply = fmap Text.pack rustTypeVarSupplyS

-- aToZsStreamed :: [Stream String]
-- aToZsStreamed = map return aToZ

-- aToZStream :: Stream String 
-- aToZStream = foldr1 interleaveStream aToZsStreamed

-- everIncreasing0s :: Stream String 
-- everIncreasing0s = unfold id ('0':) ""

pop :: Stream a -> (a,Stream a)
pop s = (headS s,tailS s)

grab :: Int -> Stream a -> ([a],Stream a)
grab 0 s = ([],s)
grab n s =
  let (rest,leftOver) = grab (n-1) (tailS s) 
  in (headS s:rest,leftOver)

-- varSupply :: Stream String 
-- varSupply = do 

--     a <- aToZStream

--     b <- everIncreasing0s
--     return (a ++ b)

natStream :: Stream Nat
natStream = unfold id Succ Zero 

{-
Similarly streaming Ints
Note that if you take from this stream too much, you will get overflow.
-}
intStream :: Stream Int 
intStream = unfold id (+1) 0

{-
For the stream of Strings, we will use simple strings.  It will start with 
  "a" -- "z"
and then it will go on to 
  a0 -- a9
and then 
  b0 -- b9
and so on out to 
  z0 -- z9
and then it will start back at 
  a00 -- ...
-}
basicDigits :: [String]
basicDigits = ["0","1","2","3","4","5","6","7","8","9"]

streamDigitString :: Stream String
streamDigitString = undefined