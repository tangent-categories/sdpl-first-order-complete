{-# LANGUAGE DeriveTraversable #-}

{-
BoundTerms uses a locally nameless representation of terms ala 
Edward Kmett's bound variable library.  It makes tasks like 
type inference and differentiation less mentally exhausting 
while also providing some nice utilities for introducing 
additional bound variables as needed in various places.
-}

module BoundCore where

import Bound 
import Control.Monad

import Data.Text (Text)

import qualified Data.Sequence as Seq
import Data.Sequence (Seq)
import Data.Bifunctor 
import Control.Monad.Morph (MFunctor(hoist))
import Data.Bifunctor (Bifunctor)

newtype CProg b a = CProg [CFunction b a] deriving (Functor,Foldable,Traversable)

instance Bifunctor CProg where 
  second = fmap 
  first f (CProg fns) = CProg (map (first f) fns)

-- A function is given by the name of the function and  binds its arguments 
-- in a term.  Since the term is locally nameless, we need only record 
-- its number of arguments.
data CFunction b a = CFunction (Int,Int,Text) !b (Scope Int (CTerm b) a) deriving (Functor,Foldable,Traversable)

instance Bifunctor CFunction where 
  second = fmap 
  first f (CFunction name b body) = CFunction name (f b) (hoist (first f) body)

data CTerm b a 
  = CVar a
  | CConst !((Int,Int),Double)
  | CZero 
  | CPlus (CTerm b a) (CTerm b a)
  | CUnit 
  | CPair (CTerm b a) (CTerm b a)
  | CScalar (CTerm b a) (CTerm b a)
  | CDot (CTerm b a) (CTerm b a)
  | CSin (CTerm b a) 
  | CCos (CTerm b a) 
  | CFst (CTerm b a)
  | CSnd (CTerm b a)
  | CExp (CTerm b a)
  | CFunCall Text  [CTerm b a]
  | CWhen [CBThen b a]
  | CBlock [CStmt b a] (CTerm b a) -- (Seq (BStmt a)) (CTerm a)
  | CLet (CTerm b a) (Scope () (CTerm b) a)
  deriving (Functor,Foldable,Traversable)

instance Bifunctor CTerm where 
  second = fmap
  first f t = case t of 
    CVar c -> CVar c 
    CConst x0 -> CConst x0 
    CZero -> CZero
    CPlus ct ct' -> CPlus (first f ct) (first f ct')
    CUnit -> CUnit 
    CPair ct ct' -> CPair (first f ct) (first f ct')
    CScalar ct ct' -> CScalar (first f ct) (first f ct')
    CDot ct ct' -> CDot (first f ct) (first f ct')
    CSin ct -> CSin (first f ct)
    CCos ct -> CCos (first f ct)
    CFst ct -> CFst (first f ct)
    CSnd ct -> CSnd (first f ct)
    CExp ct -> CExp (first f ct)
    CFunCall txt cts -> CFunCall txt (map (first f) cts)
    CWhen cts -> CWhen (map (first f) cts)
    CBlock css ct -> CBlock (map (first f) css) (first f ct)
    CLet ct sc -> CLet (first f ct) (hoist (first f) sc)


instance Applicative (CTerm b) where
  pure = CVar 
  (<*>) = ap

instance Monad (CTerm b) where
  return = CVar
  t >>= f = case t of 
    CVar a -> f a
    CConst x -> CConst x
    CZero -> CZero 
    CPlus ct ct' -> CPlus (ct >>= f) (ct' >>= f)
    CUnit -> CUnit 
    CPair ct ct' -> CPair (ct >>= f) (ct' >>= f)
    CScalar ct ct' -> CScalar (ct >>= f) (ct' >>= f)
    CDot ct ct' -> CDot (ct >>= f) (ct' >>= f)
    CSin ct -> CSin (ct >>= f)
    CCos ct -> CCos  (ct >>= f)
    CFst ct -> CFst  (ct >>= f)
    CSnd ct -> CSnd  (ct >>= f)
    CExp ct -> CExp  (ct >>= f)
    CFunCall txt cts -> CFunCall txt (map (>>= f) cts)
    CWhen cts -> CWhen (map (squishAppWhen f) cts)
    CBlock css ct -> CBlock (map (squishAppStmt f) css) (ct >>= f)
    CLet ct sc -> CLet (ct >>= f) (sc >>>= f)






{-
Rather than treat blocks as a list of statments followed by a term, we do a more classical 
interpretation through statements and the sequencing operator.  Of course to lower back 
to rust, we linearize the tree of sequences.  We could prune dead stuff, but we don't.
-}
data CStmt b a 
  = CLocalDef (CFunction b a)
  | CWhile (CBoolTerm b a) [CStmt b a]
  deriving (Functor,Foldable,Traversable)

instance Bifunctor CStmt where 
  second = fmap 
  first f s = case s of
    CLocalDef cf -> CLocalDef (first f cf)
    CWhile cbt css -> CWhile (first f cbt) (map (first f) css)


squishAppStmt :: (a -> CTerm c b) -> CStmt c a -> CStmt c b
squishAppStmt f s = case s of 
  CLocalDef cf -> case cf of 
    CFunction x0 c sc -> CLocalDef $ CFunction x0 c (sc >>>= f)     
  CWhile cbt css -> CWhile (squishAppBool f cbt) (map (squishAppStmt f) css)



data CBThen b a = CGuard (CBoolTerm b a) (CTerm b a) deriving (Functor,Foldable,Traversable)

instance Bifunctor CBThen where 
  second = fmap 
  first f (CGuard cbt ct) = CGuard (first f cbt) (first f ct)

squishAppWhen :: (a -> CTerm c b) -> CBThen c a -> CBThen c b
squishAppWhen f t = case t of { CGuard cbt ct -> CGuard (squishAppBool f cbt) (ct >>= f) } 
  

data CBoolTerm b a 
  = COr (CBoolTerm b a) (CBoolTerm  b a)
  | CAnd (CBoolTerm b a) (CBoolTerm b a) 
  | CNot (CBoolTerm b a)
  | CLess (CTerm b a) (CTerm b a)
  | CGreater (CTerm b a) (CTerm b a)
  deriving (Functor,Foldable,Traversable)

instance Bifunctor CBoolTerm where 
  second = fmap 
  first f t = case t of 
    COr cbt cbt' -> COr (first f cbt) (first f cbt')
    CAnd cbt cbt' -> CAnd (first f cbt) (first f cbt')
    CNot cbt -> CNot (first f cbt)
    CLess ct ct' -> CLess (first f ct) (first f ct')
    CGreater ct ct' -> CGreater (first f ct) (first f ct')


squishAppBool :: (a -> CTerm c b) -> CBoolTerm c a -> CBoolTerm c b
squishAppBool f b = case b of 
  COr cbt cbt' -> COr (squishAppBool f cbt) (squishAppBool f cbt')
  CAnd cbt cbt' -> CAnd (squishAppBool f cbt) (squishAppBool f cbt')
  CNot cbt -> CNot (squishAppBool f cbt)
  CLess ct ct' -> CLess (ct >>= f) (ct' >>= f)
  CGreater ct ct' -> CGreater (ct >>= f) (ct' >>= f)
