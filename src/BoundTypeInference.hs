module BoundTypeInference where

import BoundAbs
import DPLTypes
import Data.Text (Text)
import Bound
import qualified Bound.Term as BT
import qualified Data.Map as M

import Control.Unification
import qualified Data.Functor as B
import CompilerError
import Utils
import Control.Unification.IntVar
import Control.Monad.Except
import Control.Monad.State.Strict
import qualified Data.Text as T
import Data.Bits (Bits(bitSize))
import Bound.Scope (bitraverseScope)

---------------------
-- Preliminaries
---------------------

{-
Often we represent typing rules as derivations on terms in context.  However we can view them equally well as 
derivations on a kind of type closure -- that is -- the closing of its free variables and storage of the 
types that are captured in the closure (thinking of a type as a kind of value).  
   \Gamma \proves m : B 
This actually models the sense in which we take terms in context \Gamma \proves m : B to be up to alpha-renaming 
of the variables in \Gamma.  In fact, one can imagine re-arranging 
hypothetical judgments to showcase the binding as \proves (x1:A1,...,xn:An; m) : B.  (where \Gamma = (xi:Ai)_i).

Then any instantiation of the term is free to rename the variables, but the types they are assigned must be preserved.

We abstract this into the following data type (where we view a context as a pattern, hence a Scope Int) using the Bound library.
  Scope Int (BTerm sc) a: denotes a binding of Ints to variables in the BTerm.
  [a]: denotes the types of the variables in the term.  Usually, you will want to think of 'a' as Scheme b or realy Scheme IntVar.
  # Note that when calling full instantiate, it's important that the Scope was formed with respect to the order in the type scheme.
-}
data TClosure sc a = Proves (Scope Int (BTerm sc) a) [a]

-- Of course we can reconstitute a TClosure 
reconstitute :: TClosure sc a -> BTerm sc a
reconstitute (Proves sc vals) = instantiate (BVar . (vals !!)) sc -- in the case of the empty list vals, the function is everywhere undefined, and instantiate never observes it.

-- We need the ability to close the variables in a term.
-- This will fail if the variable is not in the context
closeTerm :: (Eq b,Ord b)
    => BTerm sc b
    -> M.Map b a
    -> Either CompilerError (TClosure sc a)
closeTerm t env =
    let (theFreeVars,typeSchemes) = unzip $ M.toList env in -- pull apart the key value pairs.  We can substitute by pure bound stuff.
    let fullyAbstractedTerm = abstract (listToMap theFreeVars) t in -- with all the variables fully abstracted, the term is nameless.  Thus we can change the type of the names.
    case BT.closed fullyAbstractedTerm of -- of course the map could have been lying about supplying all the variables, so we check for ourselves
        Nothing -> Left $ OutOfScopeErr "a variable was used but not defined"
        Just t_butClosed -> Right $ Proves t_butClosed typeSchemes

freeVarsTypeList :: UTerm TypeFormal a -> (Int,[a])
freeVarsTypeList ty = freeVarsTypeListAccum ty 0 []

freeVarsTypeListAccum ty sizeAcc varAcc = case ty of
    UVar a -> (sizeAcc+1,a:varAcc)
    UTerm R -> (sizeAcc,varAcc)
    UTerm TUnit -> (sizeAcc,varAcc)
    UTerm (TPair a b) ->
        let (newSize,newAcc) = freeVarsTypeListAccum b sizeAcc varAcc
        in freeVarsTypeListAccum a newSize newAcc
    -- UTerm (TArr a b) ->
    --     let (newSize,newAcc) = freeVarsTypeListAccum b sizeAcc varAcc
    --     in freeVarsTypeListAccum a newSize newAcc

{-
In DPLTypes, we defined a context to be M.Map Text (FunScheme a).
We will use this for the purpose of type inference.  This makes no distinction between function and variable names.
In fact, our syntax directly represents a representable cartesian multicategory, which is to say that constants c are in X(;B) i.e. c : -> B.
There is a syntactic difference between this and c' : () -> B in this language.  In this language c' must be defined c'(x) = m where x :().
So in this language, any definition of a constant is literally taken to be an inhabitant of X(;B).  For this reason, we may treat c and c() as syntactically 
identifical.  c'() however would not make sense.  c' requires an argument so c'(()) would make sense.  
-- Need to test this edge case.
-}




---------------------
-- Actual type inference code
---------------------

-- We return a term whose variables have their type schemes stored in them, and whose
-- local definitions are typed.
--  need to backthread through the saving of location information for better error reporting
-- Just change to return type.  Don't need to update the local definitions with types -- they are held in the environment.
-- Simplifies everything.
inferTerm :: (Monad m,Show a)
    => BTerm Text (a,FunScheme IntVar)
    -> Type
    -> ExceptT TypingFailure (IntBindingT TypeFormal (ExceptT CompilerError (StateT (TypeEnv IntVar) m))) (BTerm (FunScheme IntVar) a,Type) 
inferTerm term ty = case term of
  BVar (x,tySc) -> do
      (argTys,tyFreshed) <- fullInstantiateFunSchemeWithFresh tySc
      case argTys of
        [] -> do
            tyActual <- tyFreshed =:= ty
            return (BVar x,tyActual)
        _ -> lift $ lift $ throwArity $ show x ++ "expects " ++ show (length  argTys) ++ " arguments, but was called with none"

      -- if a variable is parsed, then the type cannot require arguments
  BConst x0 -> do
      tyActual <- real =:= ty
      return (BConst x0,tyActual)
  BZero -> do
      a <- getFreshVar
      tyActual <- a =:= ty
      return (BZero ,tyActual)
  BPlus m n -> do
      mTyVar <- getFreshVar
      nTyVar <- getFreshVar
      (m',mTy) <- inferTerm m mTyVar
      (n',nTy) <- inferTerm n nTyVar
      mTy =:= ty
      tyActual <- nTy =:= mTy
      return (BPlus m' n',tyActual)
  BUnit -> do
      tyActual <- unit =:= ty
      return (BUnit ,tyActual)
  BPair bt bt' -> do
      mTyVar <- getFreshVar
      nTyVar <- getFreshVar
      (m,mTy) <- inferTerm bt mTyVar
      (n,nTy) <- inferTerm bt' nTyVar
      tyActual <- pair mTy nTy =:= ty
      return (BPair m n,tyActual)
  BScalar r' m' -> do
      rTyV <- getFreshVar
      mTyV <- getFreshVar
      (r,rTy) <- inferTerm r' rTyV
      (m,mTy) <- inferTerm m' mTyV
      rTy =:= real
      tyActual <- mTy =:= ty
      return (BScalar r m,tyActual)
  BDot m' n' -> do
      mTyV <- getFreshVar
      nTyV <- getFreshVar
      (m,mTy) <- inferTerm m' mTyV
      (n,nTy) <- inferTerm n' nTyV
      mTy =:= nTy
      tyActual <- ty =:= real
      return (BDot m n,tyActual)
  BExp bt -> do
      mTyV <- getFreshVar
      (m,mTy) <- inferTerm bt mTyV
      mTy =:= real
      tyActual <- ty =:= real
      return (BExp m,tyActual)
  BSin bt -> do
      mTyV <- getFreshVar
      (m,mTy) <- inferTerm bt mTyV
      mTy =:= real
      tyActual <- ty =:= real
      return (BSin m,tyActual)
  BCos bt -> do
      mTyV <- getFreshVar
      (m,mTy) <- inferTerm bt mTyV
      mTy =:= real
      tyActual <- ty =:= real
      return (BCos m,tyActual)
  BFst bt -> do
      aVar <- getFreshVar
      bVar <- getFreshVar
      xVar <- getFreshVar
      (m,x) <- inferTerm bt xVar
      x =:= pair aVar bVar
      tyActual <- ty =:= aVar
      return (BFst m,tyActual)
  BSnd bt -> do
      aVar <- getFreshVar
      bVar <- getFreshVar
      xVar <- getFreshVar
      (m,x) <- inferTerm bt xVar
      x =:= pair aVar bVar
      tyActual <- ty =:= bVar
      return (BFst m,tyActual)
  BFunCall txt bts -> do
      bTyVars <- streamFreshVars (length bts)
      bsRec <- zipWithM inferTerm bts bTyVars
      let (btArgs,bTys) = unzip bsRec
      env <- lift $ lift $ lift $ gets (M.lookup txt)
      case env of
        Nothing -> lift $ lift $ throwOutOfScope $ T.unpack txt ++ " was used but not defined"
        Just funSch-> do
            -- freshen up our schemes
            (argTys,retTy) <- fullInstantiateFunSchemeWithFresh funSch
            -- check that the inferred tys match the expected tys
            zipWithM_ (=:=) argTys bTys
            tyActual <- retTy =:= ty
            return (BFunCall txt btArgs,tyActual )

  BWhen bts -> do
      btsTyVars <- streamFreshVars (length bts)
      btsI <- zipWithM inferBThen bts btsTyVars
      let (btsNew,btsTypes) = unzip btsI
      x <- getFreshVar
      mapM_ (x =:= ) btsTypes
      tyActual <- x =:= ty
      return (BWhen btsNew,tyActual)
  BBlock bss bt -> do
      s <- lift $ lift $ lift get
      bss' <- mapM inferStmt bss
      x <- getFreshVar
      (bt',btTy) <- inferTerm bt x
      tyActual <- ty =:= btTy
      lift $ lift $ lift $ put s -- the updated variable names are block local, so we restore them after leaving the block
      return (BBlock bss' bt',tyActual)
  -- BLet (BTerm b a) (Scope () (BTerm b) a)
  -- Recall that a Scope b f a is a term of type f with bound variables from b and free variables from a.
  BLet bt sc -> do -- let x = m in n == (\lambda x . n) m
          -- sc :: Scope () (BTerm b) (a,FunScheme IntVar)
      s <- lift $ lift $ lift get
      x <- getFreshVar
      (bt1,btTy) <- inferTerm bt x
      sc1 <- bitraverseScope (`tryLookup` s) (pure . fst) sc -- Scope () (BTerm (FunScheme IntVar)) a
      -- get a fresh type variable with which to instantiate the abstracted variable
    --   y <- freeVar
    --   yty <- getFreshVar
      -- pick up from here...
      let sc2Evald = instantiate1 bt sc
      -- now in
    --   y <- getFreshVar
    --   (sc1,scTy) <- inferTerm sc1 y
    --   tyActual <- ty =:= scTy
      lift $ lift $ lift $ put s
      undefined
    --   return (BLet bt1 )
  BDiffTerm n sc bts bts' -> undefined

tryLookup ::
  (Monad m, Eq k, Ord k)
  => k
  -> M.Map k v
  -> ExceptT TypingFailure (IntBindingT TypeFormal (ExceptT CompilerError m)) v
tryLookup name map = case M.lookup name map of
  Nothing -> lift $ lift $ throwOutOfScope $ "the symbol " -- ++ show name ++ " was used buy not found"
  Just a -> return a


inferStmt :: (Monad m,Show a)
  => BStmt Text (a, FunScheme IntVar)
  -> ExceptT TypingFailure (IntBindingT TypeFormal (ExceptT CompilerError (StateT (TypeEnv IntVar) m))) (BStmt (FunScheme IntVar) a)
inferStmt s = case s of
  BLocalDef f@(BFunction (row,col,name) b sc) -> do
      (fFun,fTyScheme) <- inferFunction f
      lift $ lift $ lift $ modify (M.insert name fTyScheme)
      return $ BLocalDef fFun
  BWhile bbt bss -> do
      bbt' <-  inferBool bbt
      s <- lift $ lift $ lift get
      bss' <- mapM inferStmt bss
      lift $ lift $ lift $ put s
      return $ BWhile bbt' bss'

inferFunction :: (Monad m,Show a)
  => BFunction Text (a, FunScheme IntVar)
  -> ExceptT TypingFailure (IntBindingT TypeFormal (ExceptT CompilerError (StateT (TypeEnv IntVar) m))) (BFunction (FunScheme IntVar) a, FunScheme IntVar)
inferFunction = error "not implemented"

inferBThen ::
  (Monad m,Show a)
  => BBThen Text (a, FunScheme IntVar)
  -> Type
  -> ExceptT TypingFailure (IntBindingT TypeFormal (ExceptT CompilerError (StateT (TypeEnv IntVar) m))) (BBThen (FunScheme IntVar) a, UTerm TypeFormal IntVar)
inferBThen (BGuard bbt bt) ty = do
    bbt' <- inferBool bbt
    x <- getFreshVar
    (bt',btTy) <- inferTerm bt x
    tyActual <- x =:= ty
    return (BGuard bbt' bt', tyActual)

inferBool :: (Monad m,Show a)
  => BBoolTerm Text (a, FunScheme IntVar)
  -> ExceptT TypingFailure (IntBindingT TypeFormal (ExceptT CompilerError (StateT (TypeEnv IntVar) m))) (BBoolTerm (FunScheme IntVar) a)
inferBool t = case t of
  BOr bbt bbt' -> do
      bbt1 <- inferBool bbt
      bbt2 <- inferBool bbt'
      return $ BOr bbt1 bbt2
  BAnd bbt bbt' -> do
      bbt1 <- inferBool bbt
      bbt2 <- inferBool bbt'
      return $ BAnd bbt1 bbt2
  BNot bbt -> do
      bbt1 <- inferBool bbt
      return $ BNot bbt1
  BLess bt bt' -> do
      x <- getFreshVar
      y <- getFreshVar
      (bt1,bt1Ty) <- inferTerm bt x
      (bt2,bt2Ty) <- inferTerm bt' y
      real =:= bt1Ty
      real =:= bt2Ty
      return $ BLess bt1 bt2
  BGreater bt bt' -> do
      x <- getFreshVar
      y <- getFreshVar
      (bt1,bt1Ty) <- inferTerm bt x
      (bt2,bt2Ty) <- inferTerm bt' y
      real =:= bt1Ty
      real =:= bt2Ty
      return $ BGreater bt1 bt2


---------------------
-- Cleaning up 
---------------------

fullTypeInferProgram :: BProg (Maybe ()) Text -> BProg (FunScheme b) Text
fullTypeInferProgram = undefined