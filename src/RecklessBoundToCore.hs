module RecklessBoundToCore where

-- Use the differentiation engine instead!  
-- This library just for rapid prototyping

import BoundAbs
import BoundCore

import Control.Monad.Morph

-- import Data.Sequence (Seq)
import qualified Data.Sequence as Seq

import Data.Functor

boundToCoreProgram :: BProg b a -> CProg b a
boundToCoreProgram t = case t of { BProg bfs -> CProg (map boundToCoreFunction bfs) }

{--
The monad morphism package exports the function
  hoist :: Monad m => (forall a. m a -> n a) -> t m b -> t n b 

  As Scope Int is a monad transformer and BTerm is a monad then we can hoist the monad morphism
  BTerm -> CTerm to Scope Int BTerm -> Scope Int CTerm.
-}
boundToCoreFunction :: BFunction b a -> CFunction b a 
boundToCoreFunction t = case t of { BFunction txt fnSc sc -> CFunction txt fnSc (hoist boundToCoreTerm sc)  } 

boundToCoreTerm :: BTerm b a -> CTerm b a
boundToCoreTerm t = case t of
  BVar a -> CVar a
  BConst x -> CConst x
  BZero -> CZero 
  BPlus bt bt' -> CPlus (boundToCoreTerm bt) (boundToCoreTerm bt')
  BUnit -> CUnit
  BPair bt bt' -> CPair (boundToCoreTerm bt) (boundToCoreTerm bt')
  BScalar bt bt' -> CScalar (boundToCoreTerm bt) (boundToCoreTerm bt')
  BDot bt bt' -> CDot (boundToCoreTerm bt) (boundToCoreTerm bt')
  BSin bt -> CSin (boundToCoreTerm bt)
  BCos bt -> CCos (boundToCoreTerm bt)
  BFst bt -> CFst (boundToCoreTerm bt)
  BSnd bt -> CSnd (boundToCoreTerm bt)
  BExp bt -> CExp (boundToCoreTerm bt)
  BFunCall txt bts -> CFunCall txt (map boundToCoreTerm bts)
  BWhen bts -> CWhen (map boundToCoreBWhen bts)
  BBlock bss bt -> CBlock (map boundToCoreStmt bss) (boundToCoreTerm bt)
  BLet bt sc -> CLet (boundToCoreTerm bt) (hoist boundToCoreTerm sc)
  BDiffTerm n sc bts bt's -> error "i told you not to use this function!"

boundToCoreStmt :: BStmt b a -> CStmt b a
boundToCoreStmt t = case t of
  BLocalDef bf -> CLocalDef (boundToCoreFunction bf)
  BWhile bbt bss -> CWhile (boundToCoreBool bbt) (map boundToCoreStmt bss)

boundToCoreBool :: BBoolTerm b a -> CBoolTerm b a
boundToCoreBool t = case t of 
  BOr bbt bbt' -> COr (boundToCoreBool bbt) (boundToCoreBool bbt')
  BAnd bbt bbt' -> CAnd (boundToCoreBool bbt) (boundToCoreBool bbt')
  BNot bbt -> CNot (boundToCoreBool bbt)
  BLess bt bt' -> CLess (boundToCoreTerm bt) (boundToCoreTerm bt')
  BGreater bt bt' -> CGreater (boundToCoreTerm bt) (boundToCoreTerm bt')

  

boundToCoreBWhen :: BBThen b a -> CBThen b a
boundToCoreBWhen t = case t of { BGuard bbt bt -> CGuard (boundToCoreBool bbt) (boundToCoreTerm bt)}

 