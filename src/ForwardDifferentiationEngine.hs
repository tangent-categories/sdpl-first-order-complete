
{- | 
A module whose sole responsible is to convert Language.Abs to CoreSyntax by removing all  
derivatives.  We assume terms have been pushed through a director string conversion, so that 
we can obtain the free variables in a term in amortized constant time (lookup in a set) with no traversal.
-}
module ForwardDifferentiationEngine where

import Language.Abs
import Data.Set (Set)
import qualified Data.Set as S
import Data.Map (Map)
import qualified Data.Map as M
import Data.Text (Text)
import qualified Data.Text as T
import CoreSyntax
import TermUtils

import Data.Bifunctor ( Bifunctor(first) )

import Debug.Trace

import Control.Monad.State.Strict


{-
The state of the differentiation engine contains all functions in scope as well as
all the functions added to the current scope by the differentiation engine.
-}
type DifferentiationState = (Map (Text,Nat) (CoreDecl (Set Text)), Map Text (Decl' (Set Text)))

differentialRemovalProgram :: Prog' (Set Text) -> CoreProg (Set Text)
differentialRemovalProgram p = case p of
    Program set des ->
        let
            functionMap = foldr (\decl -> M.insert (getDeclName decl) decl) M.empty des
            (declsDiffd,(funDiffsCreated,_)) = runState (mapM differentialRemovalDecl des) (M.empty,functionMap)
            newFunDiffs = map snd $ M.toList funDiffsCreated
        in
            CoreProgram set (declsDiffd ++ newFunDiffs)



-- We keep track of the names of functions that have been differentiated.
-- Since the need to differentiate a name is generated on demand, we pause, differentiate 
-- the function and store it.  Thus we also need to store the implementation.  And to be able to differentiate
-- Any function on demand, we need access to the list of functions that we started with too.  We keep track of all 
-- This information as part of the state.
-- Need to make sure that 
-- 1) Every function that requires differentiation is added to the created functions in the state
-- 2) No function that requires differentiation is added to the created functions in the state twice
--    This means we need to check and be sure of our checks that a name hasn't already been differentiated, before differentiating it
differentialRemovalDecl ::  Decl' (Set Text)  -> State DifferentiationState (CoreDecl (Set Text))
differentialRemovalDecl d = case d of
  ConstBinding set pi te -> do
      te' <- differentialRemovalTerm te
      return $ CoreConstBinding set (pIdentToCorePIdent pi) te'
  FunBinding set pi pis te -> do
    te' <- differentialRemovalTerm te
    return $ CoreFunBinding set (pIdentToCorePIdent pi) (map pIdentToCorePIdent pis) te'

differentialRemovalTerm :: Term' (Set Text) -> State DifferentiationState (CoreTerm (Set Text))
differentialRemovalTerm t = case t of
  Plus set te te' -> do
    m <- differentialRemovalTerm te
    n <- differentialRemovalTerm te'
    return $ CorePlus set m n
  Zero set -> return $ CoreZero set
  Scalar set te te' -> do
    m <- differentialRemovalTerm te
    n <- differentialRemovalTerm te'
    return $ CoreScalar set m n
  Dot set te te' -> do
    m <- differentialRemovalTerm te
    n <- differentialRemovalTerm te'
    return $ CoreDot set m n
  Unit set -> return $ CoreUnit set
  Pair set te te' -> do
    m <- differentialRemovalTerm te
    n <- differentialRemovalTerm te'
    return $ CorePair set m n
  Sin set te -> do
    m <- differentialRemovalTerm te
    return $ CoreSin set m
  Cos set te -> do
    m <- differentialRemovalTerm te
    return $ CoreCos set m
  Exp set te -> do
    m <- differentialRemovalTerm te
    return $ CoreExp set m
  Fst set te -> do
    m <- differentialRemovalTerm te
    return $ CoreFst set m
  Snd set te -> do
    m <- differentialRemovalTerm te
    return $ CoreSnd set m
  Var set pi -> return $ CoreVar set (pIdentToCorePIdent pi)
  FunCall set pi tes -> do
    args <- mapM differentialRemovalTerm tes
    return $ CoreFunCall set (pIdentToCorePIdent pi) args
  Const set num -> return $ CoreConst set (numToCoreNum num)
  When set bts -> do
    bts' <- mapM differentialRemovalBThen bts
    return $ CoreWhen set bts'
  Let set pi te te' -> do
    m <- differentialRemovalTerm te
    n <- differentialRemovalTerm te'
    return $ CoreLet set (pIdentToCorePIdent pi) m n
  BlockTerm set sts te -> do
    sts' <- mapM differentialRemovalStmt sts
    m <- differentialRemovalTerm te
    return $ CoreBlockTerm set sts' m
  DiffTerm set te pis tes tes' -> differentiateMany te pis tes tes'

differentialRemovalBThen :: BThen' (Set Text) -> State DifferentiationState  (CoreBThen (Set Text))
differentialRemovalBThen (Guard set bt te) =  do
  bt' <- differentialRemovalBool bt
  te' <- differentialRemovalTerm te
  return $ CoreGuard set bt' te'

differentialRemovalBool :: BoolTerm' (Set Text) -> State DifferentiationState  (CoreBoolTerm (Set Text))
differentialRemovalBool b = case b of
  Or set bt bt' -> do
    b1 <- differentialRemovalBool bt
    b2 <- differentialRemovalBool bt'
    return $ CoreOr set b1 b2
  And set bt bt' -> do
    b1 <- differentialRemovalBool bt
    b2 <- differentialRemovalBool bt'
    return $ CoreAnd set b1 b2
  Not set bt -> do
    b1 <- differentialRemovalBool bt
    return $ CoreNot set b1
  Less set te te' -> do
    t1 <- differentialRemovalTerm te
    t2 <- differentialRemovalTerm te'
    return $ CoreLess set t1 t2
  Greater set te te' -> do
    t1 <- differentialRemovalTerm te
    t2 <- differentialRemovalTerm te'
    return $ CoreGreater set t1 t2




differentialRemovalStmt :: Stmt' (Set Text) -> State DifferentiationState  (CoreStmt (Set Text))
differentialRemovalStmt s = case s of
  DeclStmt set de -> do
    de' <- differentialRemovalDecl de
    return $ CoreDeclStmt set de'
  While set bt sts -> do
    bt' <- differentialRemovalBool bt
    sts' <- mapM differentialRemovalStmt sts
    return $ CoreWhile set bt' sts'





{-|
This is used to force a derivative of a named function.  However, the named function might have already been differentiated,
in which case we don't need to do anything.  But if the function hasn't been differentiated, we need to differentiate 
it and then set it in our environment.  In other words, we are memoizing the differentiation of function names.
-}
differentiateName :: PIdent -> State DifferentiationState ()
differentiateName f = do
  -- we only have to check at level 1 since this the derivative of a pure function name
  (diffd,undiffed) <- get
  case M.lookup (pIdentToText f,one) diffd of
    -- the function hasn't yet been differentiated
    Nothing ->
      case M.lookup (pIdentToText f) undiffed of
        Nothing -> error "Genuine bug.  This should be possible because of an invariant of having all free function symbols declared"
        Just decl -> do
          case decl of
            -- A constant has no free variables to be differentiated, so it's zero
            (ConstBinding set pi te) -> do
              let newBinding = CoreConstBinding set (pIdentToCorePIdent pi) (CoreZero set)
              modify (Data.Bifunctor.first (M.insert (pIdentToText pi, one) newBinding))
            (FunBinding set pi pis te) -> do
              -- before differentiating we need the point and vector name lists.
              -- The easy way to create these from pis is to create a list of prepending one fixed letter
              -- and the other list of prepending a different fixed letter 
              -- ... to the same starting list.
              -- Hmm...  Is there a potential bug here...???
              -- I think there is.  Need to test but it should be possible for a programmer 
              -- to use a weird name for a global constant in the body of a function 
              -- for example avarHere where varHere is used in the body of the function and avarHere is a global constant... 
              -- one way around this would be to take the free variables in the body of the function and use that they are stored 
              -- in a set, hence ordered, pick the biggestone and then prepend a_biggestone to the ident instead of just a.
              -- TODO: modify to use the above fix 
              let pts = map (prependI 'a') pis
              let vecs = map (prependI 'v') pis
              -- fully differentiate the body
              diffdBody <- differentiateMany te pis (map identToTerm pts) (map identToTerm vecs)
              let freesBody = getFreeTerm diffdBody
              let newBinding = InternalNamedDiff freesBody (D one (pIdentToCorePIdent pi)) (map identToCorePIdent pts) (map identToCorePIdent vecs) diffdBody
              modify (Data.Bifunctor.first (M.insert (pIdentToText pi,one) newBinding))


    -- the function has already been differentiated
    Just decl -> return ()

-- just as differentiateName, but for differentiating a deriviatve (or any higher derivative)
differentiateDiffName = undefined

--data DiffName = D Int CorePIdent deriving (Eq,Ord,Show,Read)


differentiateMany :: Term' (Set Text) -> [PIdent] -> [Term' (Set Text)] -> [Term' (Set Text)] -> State DifferentiationState (CoreTerm (Set Text))
differentiateMany m ps bs vs = case (ps,bs,vs) of
  ([],_,_) -> return $ CoreZero S.empty
  ([p],[b],[v]) -> do
    m' <- differentialRemovalTerm m
    b' <- differentialRemovalTerm b
    v' <- differentialRemovalTerm v
    differentiateTerm m' (pIdentToCorePIdent p) b' v'
  -- d m / d x1 ... xn (a1 ... an)*(v1 ... vn)
  --   = x1=a1,x2=a2,...xn=an in d m / dx1 (x1)*v1 + dm/d x2...xn
  (ps@(pi : pis),bs@(b:bz),vs@(v:vz)) -> do
    let vars = map (identToCoreTerm . pIdentToIdent) (pi:pis)
    m' <- differentialRemovalTerm m
    bs' <- mapM differentialRemovalTerm bs
    vs' <- mapM differentialRemovalTerm vs
    partials <- zipWith3M (differentiateTerm m') (map pIdentToCorePIdent (pi:pis)) vars vs'
    let bigSum = summ partials
    let variableSubs = zipWith makeCoreAssignment ps bs'
    return $ CoreBlockTerm (getFreeTerm bigSum) variableSubs bigSum
  _ -> error "genuine error -- by type checking differentials must have the same number of points and vectors as the number of places differentiated"

makeCoreAssignment :: PIdent -> CoreTerm (Set Text) -> CoreStmt (Set Text)
makeCoreAssignment x a = CoreDeclStmt (getFreeTerm a S.\\ S.singleton (pIdentToText x)) $ makeConstDecl x a

makeConstDecl :: PIdent -> CoreTerm (Set Text) -> CoreDecl (Set Text)
makeConstDecl x a = CoreConstBinding (getFreeTerm a S.\\ S.singleton (pIdentToText x)) (pIdentToCorePIdent x) a

zipWith4M ::
  Monad m
  => (a -> b -> c -> d -> m e)
  -> [a]
  -> [b]
  -> [c]
  -> [d]
  -> m [e]
zipWith4M _ [] _ _ _ = return []
zipWith4M _ _ [] _ _ = return []
zipWith4M _ _ _ [] _ = return []
zipWith4M _ _ _ _ [] = return []
zipWith4M f (x:xs) (b:bs) (c:cs) (d:ds) = do
  e <- f x b c d
  es <- zipWith4M f xs bs cs ds
  return $ e:es

zipWith3M ::
  Monad m
  => (a -> b -> c -> m d)
  -> [a]
  -> [b]
  -> [c]
  -> m [d]
zipWith3M _ [] _ _ = return []
zipWith3M _ _ [] _ = return []
zipWith3M _ _ _ [] = return []
zipWith3M f (x:xs) (b:bs) (c:cs) = do
  d <- f x b c
  ds <- zipWith3M f xs bs cs
  return $ d:ds

differentiateTerm ::
  CoreTerm (Set Text)
  -> CorePIdent
  -> CoreTerm (Set Text)
  -> CoreTerm (Set Text)
  -> State DifferentiationState (CoreTerm (Set Text))
differentiateTerm t x a v
  | not (isFreeTerm (corePIdentToText x) t) = return $ CoreZero S.empty
  | otherwise = case t of
      CorePlus set ct ct' -> do
        m <- differentiateTerm ct x a v
        n <- differentiateTerm ct' x a v
        return $ CorePlus set m n
      CoreZero set -> return $ CoreZero set
      CoreScalar set ct ct' -> do
        mDiff <- differentiateTerm ct x a v
        let mSub = subst (corePIdentToText x) a ct
        nDiff <- differentiateTerm ct' x a v
        let nSub = subst (corePIdentToText x) a ct'
        return $ CorePlus set (CoreScalar set mDiff nSub) (CoreScalar set mSub nDiff)
      CoreDot set ct ct' -> do
        mDiff <- differentiateTerm ct x a v
        let mSub = subst (corePIdentToText x) a ct
        nDiff <- differentiateTerm ct' x a v
        let nSub = subst (corePIdentToText x) a ct'
        return $ CorePlus set (CoreDot set mDiff nSub) (CoreDot set mSub nDiff)
      CoreUnit set -> return $ CoreUnit set
      CorePair set ct ct' -> do
        mDiff <- differentiateTerm ct x a v
        nDiff <- differentiateTerm ct' x a v
        return $ CorePair set mDiff nDiff
      CoreSin set ct -> do
        mDiff <- differentiateTerm ct x a v
        let mSub = subst (corePIdentToText x) a ct
        return $ CoreDot set (CoreCos set mSub) mDiff
      CoreCos set ct -> do
        mDiff <- differentiateTerm ct x a v
        let mSub = subst (corePIdentToText x) a ct
        return $ CoreDot set (CoreConst set negativeOne) (CoreDot set (CoreSin set mSub) mDiff)
      CoreExp set ct -> do
        mDiff <- differentiateTerm ct x a v
        let mSub = subst (corePIdentToText x) a ct
        return $ CoreDot set (CoreExp set mSub) mDiff
      CoreFst set ct -> do
        mDiff <- differentiateTerm ct x a v
        return $ CoreFst set mDiff
      CoreSnd set ct -> do
        mDiff <- differentiateTerm ct x a v
        return $ CoreSnd set mDiff
      CoreVar set y ->
        if corePIdentToText x == corePIdentToText y
          then return v
          else return $ CoreZero set
      -- d f(m1,...,mk)/dx(a)*v = \sum_i d f(..xi..)/xi(m1[a/x],..mi[a/x]...)*d mi/x(a)*v = (let x = a in sum_i ...)
      CoreFunCall set name args -> do
        argsDiffed <- mapM (\mi -> differentiateTerm mi x a v) args
        differentiateName ((\(CorePIdent t) -> PIdent t) name)
        -- guarantee the name has been differenitated
        let partials = zipWith (flip (partialIth name set args))  [1 ..] argsDiffed
        return $ CoreLet (set S.\\ S.singleton (corePIdentToText x)) x a (summ partials)
      InternalDiffFunCall set dn cts cts' -> error "undefined"
      CoreConst set cn -> return $ CoreZero set
      CoreWhen set cbts -> error "undefined"
      CoreLet set cpi ct ct' -> error "undefined"
      CoreBlockTerm set css ct -> error "not defined yet 12" -- careful here... can't reuse differentiate decls because constants can have free variables now

partialIth :: CorePIdent -> Set Text -> [CoreTerm (Set Text)] -> CoreTerm (Set Text) -> Int -> CoreTerm (Set Text)
-- pre condition i > 0
partialIth name set ptArgs dirArg i =
  case ptArgs of
    [] -> CoreZero S.empty
    _ ->
      let
        (beforeI,afterI) = splitAt (i-1) $ replicate (length ptArgs - 1) (CoreZero S.empty)
        dirArgs = beforeI ++ dirArg:afterI
        result = InternalDiffFunCall set (D one name) ptArgs dirArgs
      in
        result


-- [
--   CoreVar 
--     (fromList ["x"]) 
--     (CorePIdent ((6,24),"x")),
--   CoreVar 
--     (fromList ["w"]) 
--     (CorePIdent ((6,26),"w")),
--   CoreVar 
--     (fromList ["b"]) 
--     (CorePIdent ((6,28),"b"))] 
-- at CorePIdent ((6,34),"x")
-- Partials
--     [
--       InternalDiffFunCall 
--         (fromList ["b","w","x"]) 
--         (D (Succ Z) (CorePIdent ((6,16),"neuron1"))) 
--         [CoreVar (fromList ["x"]) (CorePIdent ((6,24),"x")),CoreVar (fromList ["w"]) (CorePIdent ((6,26),"w")),CoreVar (fromList ["b"]) (CorePIdent ((6,28),"b"))] 
--         [CoreZero (fromList ["x"]),CoreZero (fromList []),CoreZero (fromList [])],
--       InternalDiffFunCall 
--         (fromList ["b","w","x"]) 
--         (D (Succ Z) (CorePIdent ((6,16),"neuron1"))) 
--         [CoreVar (fromList ["x"]) (CorePIdent ((6,24),"x")),CoreVar (fromList ["w"]) (CorePIdent ((6,26),"w")),CoreVar (fromList ["b"]) (CorePIdent ((6,28),"b"))] 
--         [CoreZero (fromList []),CoreZero (fromList []),CoreZero (fromList [])],
--       InternalDiffFunCall (
--         fromList ["b","w","x"]) 
--         (D (Succ Z) (CorePIdent ((6,16),"neuron1"))) 
--         [CoreVar (fromList ["x"]) (CorePIdent ((6,24),"x")),CoreVar (fromList ["w"]) (CorePIdent ((6,26),"w")),CoreVar (fromList ["b"]) (CorePIdent ((6,28),"b"))] 
--         [CoreZero (fromList []),CoreZero (fromList []),CoreZero (fromList [])]
--     ]


-- InternalDiffFunCall 
--     (fromList ["b","w","x"]) 
--     (D (Succ Z) (CorePIdent ((6,16),"neuron1"))) 
--     [CoreVar (fromList ["x"]) (CorePIdent ((6,24),"x")),CoreVar (fromList ["w"]) (CorePIdent ((6,26),"w")),CoreVar (fromList ["b"]) (CorePIdent ((6,28),"b"))] 
--     [CoreZero (fromList []),CoreZero (fromList []),CoreZero (fromList [])]


summ :: [CoreTerm (Set Text)] -> CoreTerm (Set Text)
summ ts = case ts of
  [] -> CoreZero S.empty
  _ -> foldr1 plusCS ts

plusCS :: CoreTerm (Set Text) -> CoreTerm (Set Text) -> CoreTerm (Set Text)
plusCS m n = CorePlus set m n
  where set = getFreeTerm m `S.union` getFreeTerm n

pIdentToCorePIdent :: PIdent -> CorePIdent
pIdentToCorePIdent (PIdent x) = CorePIdent x

makeFreeFromFree :: Set Text -> Text
makeFreeFromFree s =
  if S.null s
    then
      T.pack "a"
    else
      let bigName = S.findMax s in T.cons 'a' bigName

makeNFree :: Set Text -> Int -> [Text]
makeNFree s n =
  if S.null s
    then
      [T.replicate i (T.singleton 'a') | i <- [1 ..n]]
    else
      let bigName = S.findMax s in [T.append ws bigName | i <- [1 ..n],  let ws = T.replicate i (T.singleton 'a') ]


pIdentToIdent :: PIdent -> ((Int,Int),Text)
pIdentToIdent (PIdent (rc,t)) = (rc,t)

identToTerm :: ((Int,Int),Text) -> Term' (Set Text)
identToTerm (rc,t) = Var (S.singleton  t) $ PIdent (rc,t)

identToCoreTerm :: ((Int,Int),Text) -> CoreTerm (Set Text)
identToCoreTerm (rc,t) = CoreVar (S.singleton t) $ CorePIdent (rc,t)

identToCorePIdent :: ((Int,Int),Text) -> CorePIdent
identToCorePIdent (rc,t) = CorePIdent (rc,t)

prependI :: Char -> PIdent -> ((Int,Int),Text)
prependI v (PIdent (rc,t)) = (rc,T.cons v t)



numToCoreNum :: Number -> CoreNumber
numToCoreNum (Number x0) = CoreNumber x0

textToCoreNum :: Text -> CoreNumber
textToCoreNum t = CoreNumber ((0, 0), t)

negativeOne :: CoreNumber
negativeOne = textToCoreNum (T.pack "-1.0")