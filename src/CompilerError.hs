{-# LANGUAGE FlexibleContexts #-}
module CompilerError where
import Control.Monad.Except
import Control.Exception (throw)



data CompilerError = 
    LexErr String 
    | ParErr String 
    | BoundErr String -- for failure on reading Data.Text into Number -- this indicates a parser specification bug
    | OutOfScopeErr String
    | TypeUnificationError String
    | ArityError String
    | Trolled String -- for things that shouldn't be possible

instance Show CompilerError where 
    show e = case e of 
      LexErr s -> "Lexing failed with error: " ++ s
      ParErr s -> "Parsing failed with error: " ++ s 
      BoundErr s -> "Parsing text to number failed with: " ++ s ++ "! This is a parser bug.  All Numer instances should be guaranteed to be parseable to a number."
      OutOfScopeErr s -> "Analysis found a variable used out of scope.  Error report: " ++ s
      TypeUnificationError s -> "Type inference failed due to a unification error.  Error report: " ++ s
      ArityError s -> "Type inference failed due to an arity error -- a function was called with the wrong number of arguments.  Error report:" ++ s
      Trolled s -> "Trolled.  This message is only possible if a construct was forced to take a shape that is unreachable without manually constructing it.  Msg: " ++ s

-- throwLex :: (Monad m) => String -> ExceptT CompilerError m b
-- ExceptT is a MonadError so the following is more flexible
throwLex :: MonadError CompilerError m => String -> m a
throwLex msg = throwError (LexErr msg)

throwPar :: MonadError CompilerError m => String -> m a
throwPar msg = throwError (ParErr msg)

throwBound :: MonadError CompilerError m => String -> m a
throwBound msg = throwError (BoundErr msg)

throwOutOfScope :: MonadError CompilerError m => String -> m a
throwOutOfScope msg = throwError (OutOfScopeErr msg)

throwTypeUnification :: MonadError CompilerError m => String -> m a
throwTypeUnification msg = throwError (TypeUnificationError msg)

throwArity :: MonadError CompilerError m => String -> m a 
throwArity msg = throwError (ArityError msg)

throwTroll :: MonadError CompilerError m => String -> m a
throwTroll s = throwError (Trolled s)