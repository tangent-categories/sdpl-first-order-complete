{- |
A formalization of the operational semantics of sdpl.  Can be lifted to Haskabelle for formal proofs of 
    correctness and guarantees that various transformations preserve the semantics.
-}

module OperationalSemantics where

