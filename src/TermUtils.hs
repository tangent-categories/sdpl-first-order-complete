module TermUtils where

import Language.Abs
import Data.Text(Text)
import Data.Set (Set)

pIdentToText :: PIdent -> Text
pIdentToText (PIdent ((l,r),t)) = t

getDeclName :: Decl' (Set Text) -> Text
getDeclName d = case d of 
  ConstBinding _ (PIdent (n,txt)) _ -> txt
  FunBinding _ (PIdent (n,txt)) _ _ -> txt

