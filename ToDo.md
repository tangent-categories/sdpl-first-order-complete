# Todo 

## Semantics

 1. Top level symbol declarations need to be checked for semantical correctness.  We should make sure that the reference operational semantics reflects exactly the behaviour we want.
    1. There are two approaches to top level symbols with free variables in their bodies. 
       1. One is that they represent symbols that must be imported from elsewhere.  On import -- the types should be imported and checked too.
       2. The other is that the represent existential statements.  The top level entities are type checked with free variables being lifted and also given types -- the types of the free variables are then given a namespacing relative to the module and declaration name.  If a declaration containing free variables is used, then the user inherits a free variable existential as well -- this is preserved upon imports.  The chaining of free variables is allowed to propagate with one exception -- the special function named main.  Main must supply instances for all free variables.  
          1. In order to perform this we need syntax extension.  We need special support for exporting symbols.  We also need a special support that an implementation is providing a freevariable.  
          2. For example: Suppose the following are top level declarations.
            x = a
            y = x+x
            Then x : V \exists a:V
                 y : V \exists a:V
            And to see resolution
            z = 3+y with a = 5
            Simply has 
                 z : R
            But this does not propagate and force a type constraint onto x,y.  It should still be possible to do
            w = b + y
            And get 
                 w : U \exists b:U,a:U
            And even 
            t = (3,3) + w 
                 t: (R,R) \exists b : (R,R),a:(R,R)
            And of course
            m = t + t with b=(3,3),a=(4,4)
            And get 
                  m : (R,R)
 2. Import statements, their full semantics in the reference operational semantics, and checking that we adhere to that semantics.

## Compiler infrastructure cleanup
Move all scripts to scripts/ directory.