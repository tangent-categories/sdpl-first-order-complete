#! /usr/bin/env nix-shell
#! nix-shell -i bash --pure shell.nix

# First generate the cabal file from the package.yaml using hpack
hpack package.yaml

# Then build the program
cabal new-build

