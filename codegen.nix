# we need an 'experimental' rust feature for the build to succeed so we use the official mozilla overlay to get nightly rust.
let
  moz_overlay = import (builtins.fetchTarball https://github.com/mozilla/nixpkgs-mozilla/archive/master.tar.gz);
  nixpkgs = import <nixpkgs> { overlays = [ moz_overlay ]; };
in
  with nixpkgs;
  stdenv.mkDerivation {
    name = "moz_overlay_shell";
    buildInputs = [
      # to use the latest nightly:
      nixpkgs.latest.rustChannels.nightly.rust
    ];
    shellHook=''
    # The following is used to correctly set the locale to unicode.
      export LC_ALL=C.UTF-8
    '';
    
  }