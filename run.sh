#! /usr/bin/env nix-shell
#! nix-shell -i bash --pure shell.nix

cabal new-run SDPL-First-Order-Complete-exe $1
mkdir -p .sdpl-build
mv sources.rs .sdpl-build/
cp rust-aux/stdlib.rs .sdpl-build/stdlib.rs
cp rust-aux/main-chopped.rs .sdpl-build/main-chopped.rs
cp codegen.nix .sdpl-build/codegen.nix
cd .sdpl-build
cat main-chopped.rs sources.rs > main.rs
cp rust-aux/rustRunner.sh .sdpl-build/rustRunner.sh
echo ""
echo "Adjust main file in .sdpl-build/main.rs to run the function of your choice"
echo "Then run rustRunner.sh in that folder"
echo ""
