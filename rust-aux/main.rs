#![feature(trait_alias)]




//TODO:
//Create turtle shell script to define wrap a module with a main function
//change the type of main to main () from main () -> (), 
// do a print on the last line of main to the last value output
// add the #![feature(trait_alias)]
// copy the stdlib to a subfolder with the resulting program
// and create the import statements required.
pub mod stdlib;
use stdlib::*;

fn h(x:f64,y:f64) -> f64 where 
{
	x+y
}


fn sigmoid (x : f64) -> f64 where
{
  sin (x)
}
fn neuron1 (x : f64, w : f64, b : f64) -> f64 where
{
  Plus::plus (b, sin (Dot::dot (x, w)))
}
fn g (a : f64, v : f64, w : f64, b : f64) -> f64 where
{
  {
    let x = a;
    Plus::plus (__1neuron1 (x, w, b, v, Zero::zero(), Zero::zero()), (Plus::plus (__1neuron1 (x, w, b, Zero::zero(), Zero::zero(), Zero::zero()), __1neuron1 (x, w, b, Zero::zero(), Zero::zero(), Zero::zero()))))
  }
}
fn __1neuron1 (ax : f64, aw : f64, ab : f64, vx : f64, vw : f64, vb : f64) -> f64 where
{
  {
    let x = ax;
    let w = aw;
    let b = ab;
    Plus::plus (Plus::plus (Zero::zero(), Dot::dot (cos (
    {
      let x = x;
      Dot::dot (x, w)
    }
    ), (Plus::plus (Dot::dot (vx, (
    {
      let x = x;
      w
    }
    )), Dot::dot ((
    {
      let x = x;
      x
    }
    ), Zero::zero()))))), (Plus::plus (Plus::plus (Zero::zero(), Dot::dot (cos (
    {
      let w = w;
      Dot::dot (x, w)
    }
    ), (Plus::plus (Dot::dot (Zero::zero(), (
    {
      let w = w;
      w
    }
    )), Dot::dot ((
    {
      let w = w;
      x
    }
    ), vw))))), (Plus::plus (vb, Zero::zero())))))
  }
}


fn main () {
	println!("hello world");
	let z:f64 = Zero::zero();
	println!("{}",z);
	let x:(f64,f64) = Zero::zero();
	println!("{}",Show::show(&x));
	let w:(f64,f64) = Plus::plus((1.0,2.0),(3.0,4.0));
	println!("{}",Show::show(&w));
	let a:(f64,((),f64)) = Scalar::scalar(1.1,(2.0,((),3.4)));
	println!("{}",Show::show(&a));
	let b:f64 = Dot::dot(  (1.1,2.2) , (3.1,4.1)  );
	println!("{}",Show::show(&b));
	println!("{}",h(1.0,2.0));
	let c :f64 = g(1.5,2.1,3.10,4.10);
	println!("{}",c);
	let d:f64 = __1neuron1(1.1,1.2,1.3,1.4,1.5,1.6);
	println!("{}",d);
	let e:f64 = rt(1.1,1.1);
	println!("{}",e);
}

fn rt(x:f64,y:f64) -> f64 {
	x*y
}