
// #![feature(trait_alias)]

//This allows changing the "base" field used in the backend, as long as R has addition and multiplication.
//We could probably do some stronger typing here, but we'll forego this for now because we'd have to change 
// the backend to target this adding unneeded complexity.
// enum R {
// 	R(f64)
// }

pub trait Zero {
	fn zero() -> Self;
}

impl Zero for () {
	fn zero() -> Self {
		()
	}
}

impl Zero for f64 {
	fn zero() -> Self {
		0.0
	}
}

impl<A:Zero,B:Zero> Zero for (A,B)  {
	fn zero() -> Self {
		(Zero::zero(),Zero::zero())
	}
}

pub trait Show {
	fn show(&self) -> String;
}

impl Show for () {
	fn show(&self) -> String {
		let out = std::string::ToString::to_string("()");
		out
	}
}

// impl Show for R {
// 	fn show(&self) -> String {
// 		match self {
// 			R::R(x) => {
// 				let out = std::string::ToString::to_string(x);
// 				out
// 			}
// 		}
// 	}
// }

impl Show for f64 {
	fn show(&self) -> String {
		let out = std::string::ToString::to_string(self);
		out
	}
}

impl<A:Show,B:Show> Show for (A,B) {
	fn show(&self) -> String {
		let outl = Show::show(&self.0);
		let outr = Show::show(&self.1);
		let out = format!("({},{})",outl,outr);
		out
	}
}


pub trait Plus {
	fn plus(x:Self,y:Self) -> Self;
}

impl Plus for () {
	fn plus(_x:Self,_y:Self) -> Self {
		()
	}
}

impl Plus for f64 {
 	fn plus(x:Self,y:Self) -> Self {
 		x+y 
 	}
}

impl<A:Plus,B:Plus> Plus for (A,B) {
	fn plus(x:Self,y:Self) -> Self {
		(Plus::plus(x.0,y.0),Plus::plus(x.1,y.1))
	}
}

pub trait Scalar {
	fn scalar(f:f64,v:Self) -> Self;
}

impl Scalar for (){
	fn scalar(_x:f64,_y:Self) -> Self {
		()
	}
}

impl Scalar for f64 {
	fn scalar (f:f64,v:Self) -> Self {
		f*v
	}
}

impl<A:Scalar,B:Scalar> Scalar for (A,B) {
	fn scalar (f:f64,v:Self) -> Self {
		(Scalar::scalar(f,v.0),Scalar::scalar(f,v.1))
	}
}

pub trait Dot {
	fn dot (v:Self,w:Self) -> f64;
}

impl Dot for () {
	fn dot (_v:Self,_w:Self) -> f64{
		0.0
	}
}

impl Dot for f64 {
	fn dot (v:Self,w:Self) -> f64 {
		v*w
	}
}

impl<A:Dot,B:Dot> Dot for (A,B) {
	fn dot (v:Self,w:Self) -> f64 {
		Dot::dot(v.0,w.0) + Dot::dot(v.1,w.1)
	}
}


// Sometimes you just have to wonder what they were thinking
pub fn sin(x:f64) -> f64 {
	x.sin()
}

pub fn cos(x:f64) -> f64 {
	x.cos()
}

trait Dpl = Show + Zero + Plus + Scalar + Dot; 