#! /usr/bin/env nix-shell
#! nix-shell -i bash --pure bnfc.nix

# This script fetches bnfc and happy from nix-packages, then
#   runs bnfc on the correct file with the correct options
#   then runs happy on the generated grammar and extracts the 
#       ambiguity report 
#   and then finally cleans up.
#   No stateful change is made to the users' system with this method.

# Rebuild the cf file with the correct options and then build the project
cd src
bnfc --haskell -d --text-token --functor SubRust.cf
# go into the SubRust Language folder
cd SubRust

happy --info=grammar-ambigs-backend.txt --outfile=grammar.out Par.y
# remove the grammar.out file; it's not needed
rm grammar.out
# leave the directory
cd ..
# we're now back in the src directory
# let's move the grammar-ambigs to the project root 
mv SubRust/grammar-ambigs-backend.txt ../grammar-ambigs-backend.txt

# Remove the unused file that also breaks the build
rm SubRust/Test.hs
