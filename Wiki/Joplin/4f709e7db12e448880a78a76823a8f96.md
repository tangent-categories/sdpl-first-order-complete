5. Privacy policy

# Privacy policy

Joplin values your privacy by giving you complete control over your information and digital footprint.

Joplin applications do not send any data to any service without your authorisation. Any data that Joplin saves, such as notes or images, are saved to your own device and you are free to delete this data at any time.

Joplin has many modern features, some of which use third-party services. You can disable most of these features in the application settings. These features are:

| Feature  | Description   | Default  | Can be disabled |
| -------- | ------------- | -------- | --- |
| Auto-update | Joplin periodically connects to GitHub to check for new releases. | Enabled | Yes |
| Geo-location | Joplin saves geo-location information in note properties when you create a note. | Enabled | Yes |
| Synchronisation | Joplin supports synchronisation of your notes across multiple devices. If you choose to synchronise with a third-party, such as OneDrive, the notes will be sent to your OneDrive account, in which case the third-party privacy policy applies. | Disabled | Yes |
| Wifi connection check | On mobile, Joplin checks for Wifi connectivity to give the option to synchronise data only when Wifi is enabled. | Enabled | No <sup>(1)</sup> |

(1) https://github.com/laurent22/joplin/issues/5705

Joplin is developed as an open-source application and the source code is freely available online to inspect.

For any question about Joplin privacy, please leave a message on the [Joplin Forum](https://discourse.joplinapp.org/).


id: 4f709e7db12e448880a78a76823a8f96
parent_id: d9c6c5b5302546deb0a27816b67ea086
created_time: 2022-02-07T00:05:24.017Z
updated_time: 2022-02-07T00:05:24.017Z
is_conflict: 0
latitude: 0.00000000
longitude: 0.00000000
altitude: 0.0000
author: 
source_url: 
is_todo: 0
todo_due: 0
todo_completed: 0
source: joplin-desktop
source_application: net.cozic.joplin-desktop
application_data: 
order: 1644192324017
user_created_time: 2022-02-07T00:05:24.017Z
user_updated_time: 2022-02-07T00:05:24.017Z
encryption_cipher_text: 
encryption_applied: 0
markup_language: 1
is_shared: 0
share_id: 
conflict_original_id: 
master_key_id: 
type_: 1