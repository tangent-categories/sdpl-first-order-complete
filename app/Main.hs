module Main where


import Language.Lex
import Language.Par
import qualified Language.Print as LPrint

import Data.Text ()
import qualified Data.Text as T
import qualified Data.Text.IO as TI



import qualified SubRust.Print as RPrint


-- uncomment the following if you need to debug unicode issues on your platform
import GHC.IO.Encoding(getLocaleEncoding,setLocaleEncoding)

import System.IO
import System.Environment ( getArgs )

import ForwardDifferentiationEngine

import qualified SubRust.Print as CodeGen
-- import AbsToBoundAbs
-- import RecklessBoundToCore -- Replace this with differentiation engine
-- import BoundCoreToSubRust
import Language.Abs
import CoreSyntax
import Directorize
import CoreToSubRustFake
import Lib
import Control.Monad.Except (runExcept)
import Data.Foldable
-- import TypeInference (defaultTextVarSupply, pIdentToText)
import Control.Monad.State.Strict (evalState)
-- import Stream
-- import Control.Monad.Morph (MFunctor(hoist))

import Data.Bifunctor

main :: IO ()
main = do
    m <- getLocaleEncoding
    print m -- this often prints ASCII which is not a good thing.  However, if run in the shell.nix, the export LC_ALL statement should fix this.
    setLocaleEncoding utf8
    n <- getLocaleEncoding
    print n
    args <- getArgs
    case args of
      [fileName] -> do
        -- putStrLn "enter next term"
        fileContents <- TI.readFile fileName
        let tokens = myLexer fileContents
        print tokens
        case pProg tokens of
          Left s -> putStrLn s
          Right prog -> do
            let (directedProg,_) = directProg prog
            print (LPrint.printTree directedProg)
            let differentiatedProg = differentialRemovalProgram directedProg
            let rustProg = coreToSubRustProg differentiatedProg
            let rustProgToLower = CodeGen.printTree rustProg
            
            TI.writeFile "sources.rs" (T.pack rustProgToLower)
      _ -> putStrLn "\nUsage: sdpl Filename.sdpl\nOutput: Rust-sources"
    



    putStrLn "Done"

