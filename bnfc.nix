{ pkgs ? import <nixpkgs> {}}:
let 
  happy = pkgs.haskellPackages.happy;
  bnfc = pkgs.haskellPackages.BNFC;
in 
  pkgs.mkShell {
    buildInputs = [ happy bnfc ];


    shellHook=''
    # The following is used to correctly set the locale to unicode.
      export LC_ALL=C.UTF-8
    '';
    

  }