# cleanup the build artifacts
yes | rm -r dist-newstyle
cd src

# cleanup the bnfc frontend build artifacts
yes | rm -r Language

# cleanup bnfc backend build artifacts
yes | rm -r SubRust

cd ..

# cleanup the generated cabal file
rm *.cabal
