# Getting started

We've tried to make getting started on hacking on DPL as easy and streamlined as possible.  Our goal is to provide a developer experience that is smooth enough that someone familiar with Haskell and BNFC can be up and running on any parts of this project **within 15 minutes**.  Please let us know if there are any issues that prevent you from being up and functional **within 15 minutes**.  If not working on the front or backends, one only needs basic Haskell knowledge.
 
# Hardware setup 
This section must be completed if: 
  * Running on Windows.
  * Running on MacOS and using the Quickstart environment.  For MacOS, one can alternatively use a pure Nix environment (see further below).

To set the hardware options boot the computer into BIOS/UEFI or edit the coreboot settings.  
  * For Intel processors, turn on VT-x (aka Intel virtualization technology) and VT-d (aka virtualization for IO aka IOMMU)
  * For AMD processors, turn on AMD-V (aka AMD virtualization) and AMD-Vi (aka AMD IOMMU)
  * Typically to change these settings in BIOS/UEFI, press F2, F10 or F12 during boot, select the option that corresponds roughly to "enter BIOS or enter UEFI or possibly change/edit BIOS/UEFI settings".  Alternatively search for "change BIOS settings `<Your Computer Model>`" on the internet.
    * After getting to the 1990s-esque interface, search around for the virtualization settings.
    * If you're using coreboot, do the coreboot thing to change settings.



**Note 1:** There are a few security risks one should be aware of with enabling hardware virtualization support.  One risk is that an attacker has root privilege and can setup a hypervisor-rootkit; this attack requires ring 0 privilege escalation so you're pwned anyways.  Another is in the case that there is a bug in the virtual machine monitor that allows a VM to leak information because of improper isolation.  There may be other risks.

**Note 2:** This section can be skipped on Windows if one uses a non-supported dev environment on Windows and does not use Nix nor docker.  This situation is not actually too bad, but does require some fiddling to get a nice developer experience; see the `Pure Build [Unsupported]` section below.
This is the only method available for pure Windows builds (i.e. that want to avoid WSL2 entirely).  It is also the only method for a pure OSS environment on Windows.




# Quickstart  [Supported,Non-OSS]
To get up and hacking on sdpl as fast as possible, we have provided a devcontainer environment for VSCode that sets up your editor, build environment, and project so that you can start coding immediately.
For other set up methods or to use only OSS development environments read past this section.

Pre-requisites:
  - Docker and docker-compose are installed 
  - VSCode and the remote-container extension are installed
  - Docker can be run as a regular user

After meeting the pre-requisites, then just run VSCode, and select `File > Open Folder`.  If the menu bar (containing File) is hidden, then hit `Alt` to show it.  Then open the folder containing the git repository [e.g. if you did a `git clone git@gitlab.com:tangent-categories/sdpl-first-order-complete.git` then open the folder `sdpl-first-order-complete`].

Allow up to a few minutes for all the dependencies to be pulled in and for the project to complete an initial build.
Then you're up and away.

## Pre-requisites

To **install Docker:**  
  - https://docs.docker.com/get-docker/

Note: for Windows and MacOS, the default installations will install Docker Desktop, a free-for-personal use nonOSS tool.  For Linux distros, the docker installation will give OSS tooling only.
  - For Ubuntu: https://docs.docker.com/engine/install/ubuntu/
  - For Arch Linux based (Arch, Majaro, EndeavourOS): `sudo pacman -Syyu && sudo pacman -S docker docker-compose`
  - For Debian: https://docs.docker.com/engine/install/debian/
  - For Fedora: https://docs.docker.com/engine/install/fedora/

**Install docker-compose:**
  - For Windows/MacOS: docker-compose is installed by Docker Desktop.
  - For non Arch based Linux: https://docs.docker.com/compose/install/
    - Scroll down to "Install Compose" and find the tabs, Mac|Windows|Windows Server|Linux|Alternative... and then select the Linux tab and follow the instructions.
  - For Arch Linux based, you can use the general Linux install or just `sudo pacman -S docker-compose`.

**Install  VSCode:**
  - https://code.visualstudio.com/download
  - For *Ubuntu, one can install VSCode from the Snap store
  - For Arch based Linuxes one can install VSCode from AUR.  https://aur.archlinux.org/packages/visual-studio-code-bin.
    - You cannot use `sudo pacman -S code` to install VSCode for this solution.  Doing so installs only the OSS version of VSCode, and the remote extensions are incompatible with the OSS version.  

**Install the remote containers extension:**
  - https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers

**Set to run as regular user**
- For Windows and MacOS using Docker Desktop this is already done.
- For WSL2 under windows follow the instructions for Linux.
- For Linux based operating systems run: `sudo usermod -aG docker $(whoami)`.

**Note** For windows with WSL2 installed, there is a way around installing Docker Desktop, if that is desired.  However, this is probably considered experimental and could break.  To allow fully unprivileged and unmodified users to run docker, the docker team have a guide on installing and running as an unprivileged user and can be accomplished without systemd.  It's the "without systemd" part that is of interest here because WSL2 can't easily run systemd (systemd is a special program (init/PID 1) and isn't currently supported by official docker guidance.  Anyways, find details here https://docs.docker.com/engine/security/rootless/ to set up a rootless installation; it's involved but straightforward.  Then scroll to the "Usage" section, and then under "Daemon" there are two tabs: "With systemd" and "Without systemd".  Choose the "without systemd" tab, and follow the instructions. 

# Quickstart platform specific status

**Tested and works:**
  * Windows pure 
    * Manual build with hpack, bnfc and cabal-install [not supported]
  * Windows with Docker Desktop and WSL2 backend
    * Quickstart works out of the box.
    * Manual docker-based build through WSL2 and Docker Desktop installed (installing Docker Desktop makes the docker tool available for WSL2).
    * Pure nix script builds
    * Manual build with hpack, bnfc and cabal-install (though not supported)
  * Windows with WSL2 only + VT-x
  * Linux based
    * With or without  VT-x
    * With Docker
      * Quickstart, manual docker, nix work 
    * With Nix
      * Nix builds
    * Pure 
      * hpack, bnfc, and cabal-install manual builds 
  * Linux based on VMWare:
      * With VT-x and VT-d enabled in Host and VT-x/EPT or AMD-V/RVI in VMWare machine settings
        * Same as standard Linux
  * Linux based on Virtualbox:
    * With VT-x and VT-d enabled in Host and Nested virtualization and nested page tables in Virtualbox machine settings.
      * Same as standard Linux.  Note IOMMU passthrough is experimental and only for Linux hosts.  Doesn't seem to affect this use case, as Docker on Linux isn't nesting virtualization.
  * Linux based on Hyper-V
    * VT-x and VT-d enabled in Host.  During initial machine setup, unselect "Use dynamic memory for this virtual machine" (under the Assign Memory dialog).  After machine creation, so that the machine has a name <Name> run `Set-VMProcessor -VMName <Name> -ExposeVirtualizationExtensions $true`.  
      * Same as standard Linux.  Hyper-V CPU and IOMMU virtualization is complicated, probably because Microsoft has done some pretty deep digging into VM issues.  ExposeVirtualizationExtensions as above is sufficient so that if "intel_iommu=on" or "amd_iommu=on" is added in the `/etc/default/grub` to the `GRUB_CMDLINE_LINUX_DEFAULT` options and "options kvm_amd nested=1" is added to `/etc/modprobe.d/kvm.conf` that IOMMU is passed through to the guest (confirmed with `sudo dmesg | grep -i dmar`) and nested virtualization in kvm is turned on (confirmed with `cat /sys/module/kvm_intel/parameters/nested`).

**Windows only:**
  * Install WSL2: https://docs.microsoft.com/en-us/windows/wsl/install
  * Install docker desktop: https://docs.docker.com/desktop/windows/install/
    * After install find the "Use WSL2 Backend" in the settings and make sure it is turned on.


**Not Tested**
  * MacOS with Intel Hardware
    * Pure nix script builds probably work
    * Manual docker build probably works with docker desktop installed
    * Devcontainer build with VSCode probably works with docker desktop installed
    * Pure cabal-builds with hpack, bnfc, and cabal-install
  * MacOS with M1 hardware
    * No idea, please test 
  * Windows with Docker Desktop and non WSL2 backend
    * No idea, please test
  * Windows with WSL2 only and no VT-x
    * Does WSL2 still require Hyper-V with VT-x enabled?  Is it still running as a level 1 hypervisor, hence needs hardware virtualization support?
    * 
    * 
    * dy:  https://docs.microsoft.com/en-us/windows/wsl/install
    - Install docker desktop: https://docs.docker.com/desktop/windows/install/
      - After install find the "Use WSL2 Backend" in the settings and make sure it is turned on.
  - Windows with Linux installed as a Guest in VMWare/Virtualbox/Hyper-V:
    - Make sure hardware virtualization is turned on (see above)
    - Enable nested virtualization in the virtual machine settings:
      - For VMWare: turn on "Virtualized VT-x/EPT or AMD-V/RVI" and turn on "virtualize IOMMU" in the machine settings 
      - For Virtualbox: turn on "enable nested virtualization".  Caveat: it might not be possible to pass the IOMMU through.  At present, it's experimental and only for Linux hosts.  That said, it shouldn't cause a problem for this particular use case.
      - For VMWare and Virtualbox: these tools used to be incompatible with Hyper-V.  Recently, this is seeming to change.  If you run into strange errors, look into turning Hyper-V features off.
      - For Hyper-V: Make sure that during the initial machine setup, that "Use dynamic memory for this virtual machine" (under Assign Memory) is turned off.  Then at a powershell with admin rights prompt, enter `Set-VMProcessor -VMName L1-VM -ExposeVirtualizationExtensions $true`.  Then run the virtual machine and install a Linux distro.
    - Follow distro specific guidance for 


# Continue from here

# Nix environment [Supported, OSS]
This will guide you through setting up the development with pure nix, and setting up the VSCode editor interact with Nix.  

# Docker environment [Supported, OSS]
This will guide you through setting up development with pure docker and not the devcontainer environment.  The reason one might choose this is to have more fine-grained control over their set-up, to understand how the devcontainer environment is put together (maybe you prefer ghcid or some kind of continuous build flow), or to write one's own devcontainer environment, or to use a pure OSS build setup without needing on VSCode nor Nix.  

## Exercise version
If you're learning docker, a nice exercise version of this set up is:

  1) Find the nixos/nix docker container
  2) Write a docker file that copies the nix shell into the docker container 
  3) Create an isolated container volume or mount a host file system to get the data into the container in a persistent way (the container can be torn down and brought back up without affecting the code/data).  See https://code.visualstudio.com/docs/remote/containers
  4) make a docker-compose.yml that brings up the correct docker containers and volumes
  5) if okay with nonOSS, open the created container in VSCode + container extension.  Once opened in the container, follow the the instructions for Nix-environment as above
  6) If only OSS software is desired: note that one can run the shell interactively `docker run --rm -it sdpl /bin/sh` and then once "inside" the shell, one can install stuff like in a normal NixOS installation.  In particular, one can do `nix-env -iA emacs` or `nix-env -iA vim` to install emacs or vim, and you can edit directly in the container.  Alternatively, if you're okay with running an ssh server in your host, then you can connect to the docker image through ssh, and edit files in the container from any editor that supports connections over ssh.

## Complete walkthrough

# Pure build [Unsupported,OSS]
In the nix environment, we saw that all of the functionality comes from scripts we wrote to automate build tasks and the scripts use Nix to provide shells with the required tools and dependencies.  We also used the Nix-shell integration in VSCode to allow the full IDE features of VSCode to work when the underlying tools are only available in the nix shell.

However, the scripts can be easily ported to not require nix.  Manually install at least ghc and cabal-install.  Then cabal install the required dependencies in the project root with sandboxing turned on.  The required dependencies are hpack, bnfc, alex and happy.  With these installed, the build scripts and cabal can handle the rest. It should be possible to port the build scripts by simply removing the Hashbangs (`#!`)s from the top of each script and replacing it with your shell .e.g `#!/bin/bash` (note there are typically two (`#!`)s in each script and they should both be removed).
  
## Reproducibly port the pure build [Todo]
For each script, write the non-nix version that works in a pure build.  Change the full build/initialization script to setup a sandbox (is this done by default now?), and ensure cabal install only installs to within this sandbox. Write nix versions of the scripts that wrap the non-nix versions by doing something like `nix-shell --pure shell.nix --command "pure-script.sh"`.  

Make it easy to change the dev environment to using a different base docker image with or without nix.


## Nix environment  [Supported, OSS]
To install nix: details can be found at: https://nixos.org/download.html.  Choose the install method that suits your needs -- make sure to reboot after installing nix.  Using nix gives a consistent build semantics on most Linux-based systems, *BSDs, MacOS, and Windows (under WSL2).

Once nix is installed, get the tooling up and running: type 

    nix-shell shell.nix

from the main project directory.  This will pull in most of the Haskell dependencies we need to build the project.  This means that there is no need to install any Haskell, Rust, LLVM, nor BNFC tooling in the main system.

Then exit the nix shell by typing 

    exit

Now we're all set to perform a first build of the compiler.  Run the full build script:

    ./full-compiler-build.sh

This performs the following actions:

    1) syncs the cabal package list
    2) builds the compiler frontend infrastructure using bnfc
    3) builds the compiler backend infrastructure using bnfc
    4) builds the main compiler body which does
       1) generates a cabal project from the package.yaml file 
       2) uses cabal to build the project

Since this project uses cabal version 2 builds, the main build should be sandboxed and the artifacts stored in dist-newstyle.

If the frontend is changed, rebuild the frontend with `build-bnfc-frontend.sh`.  If the backend is changed rebuild with `build-bnfc-backend.sh`.  The main compiler body left is then a .cabal project, which can be built from the Haskell tooling integrated in most modern editors, and can also be built manually with `cabal new-build` or even just `cabal build` -- however be sure to perform this in the nix-shell.  In other words,

    1) nix-shell --pure shell.nix
    2) cabal build

The `--pure` ensures a bit more build isolation as all but the most essential environment variables are removed from the nix shell.

Please report any errors if this setup does not work out of the box, as this is our main development base.  Also inform us if we should change the extensions we're using.

## Docker environment [Supported]
For those that do not wish to install `nix` on their system but are comfortable with docker, then you can use our docker image to build this project.  This docker image sets up the nix tooling and then uses the above methodology.  On an initial docker build, we recommend running 

    docker build -f sdpl.Dockerfile -t sdpl .

This creates a docker image that contains nix and pre-installs all the nix packages we need to build our project.

One can then interactively run the docker environment with 

    docker run --rm -it sdpl /bin/bash

At this point, one is in a normal shell where one can follow the instructions for the Docker environment as above.

However, there's a problem: there's no actual code to build in the container!  This is an intentional decision -- one should be able to tear down the container and rebuild it while not affecting any changes that we hadn't pushed to git yet.  Here are some solutions for getting data into the container.  

Please report any errors if this does not work out of the box.

### Use an isolated container volume
We have included two scripts in this repo to automate the task of setting up an isolated container volume.  `initVolumeClean.sh` and `initVolumeFromGitRoot.sh`
The former creates a fresh docker volume called `sdpl_git_repo` and pulls in the git sources to it.  The latter must be run from the project root (so `scripts/initVolumeFromGitRoot.sh`), and creates a fresh docker volume called `sdpl_git_repo` with the current git repository loaded into it.

Note, changes made to files in the git repo stored in the docker volume will not be reflected on the host system.  However, one can add, commit, push, etc from within a docker container.  After either script is run, you can then run the docker image as 

    docker run --rm -it --volume=sdpl_git_repo:/sdpl_git_repo:rw


### Mount local filesystem into image

It is also possible to directly mount a local filesystem into the docker container.  Done this way, changes in host or container are shared.  To mount a local filesystem into the docker image use and run interactively use:

    docker run --rm -it --volume=$PATH_TO_LOCAL_REPO/:/sdpl_git_repo:rw

Then the local repo will be in the folder `/sdpl_git_repo` in the container.  One acknowledges that some isolation benefits are lost under this method.

## Devcontainer environment [Supported]
This method is the most reproducible and fast.
The third method builds on the second method by using the VSCode docker integrations to create a "devcontainer."  If one wants a completely automated setup just open the git repository in VSCode and say "yes" when asked to run in container.  VSCode will set up everything automatically (see more on editor support below).  It is also set up so that every aspect of the build can be performed within VSCode.


## Troubleshooting Nix or Docker build
If you run `cabal new-build` or `cabal build` at the commandline, you may see an error about ghc or cabal not being installed.  You need to enter the nix shell to use these commands.  Type 

    nix-shell shell.nix
    cabal build

and the error will go away.

If, when using `cabal build`, you see a bunch of packages listed for installation, then you're not in the nix-shell.  This means you have cabal and ghc globally installed in your system and are attempting to build the project using global tooling.  This will probably work, but it doesn't create an isolated and reproducible environment which might not be a problem, but it could be.  N.B., cabal may need to install alex and happy on a first run, but no other dependencies should need to be pulled in by cabal.  If dependencies other than alex or happy are required, please report this as a bug.

Note: Bug reports that cannot be reproduced when built with nix-tooling or docker-tooling will be dismissed.


## Generic build instructions
Regardless of the development environment, there are some general build instructions and notes.

This project is set up to be buildable as a pure cabal-based build.  However, notice that there is no .cabal file included in the git repository -- this is because we synthesize the .cabal file from the package.yaml file using hpack.  The package.yaml file contains  less redundant information than the .cabal specification; hpack intelligently generates the .cabal by filling in module information from the project.  

There are three main parts to the compiler that have to be built:
  1) The frontend
  2) The backend
  3) The main compiler

The only time the frontend or backend needs to be recompiled is when changes are made to either Language.cf or SubRust.cf.
The Language.cf file contains the specification of the frontend of the language -- we use the lexer and parser that are generated by bnfc.  The SubRust.cf file contains the specification of the sublanguage of Rust that we compile to -- we use the pretty printer generated by bnfc to perform code lowering.  For the most part, changes will be made to the main compiler, and will not require running the scripts -- only rebuilding the cabal project is necessary.  

We have included scripts for building the front and backends, and these scripts rely on nix.  Non-nix builds are not supported; however, if all the required tools are available, the scripts should be able to be repurposed for building without nix: just remove the "hashbang" lines (`#!`)  from the top of the scripts and set the desired shell instead (e.g. `#!/bin/bash`).  

## VSCode local only [OSS]
The first way to setup an editor is to use VSCode and a few extensions.  After running the `full-compiler-build.sh` script at the commandline once, the project is setup to use VSCode with a few extensions.  

First install VSCode.  For this method, since no remote containers, etc are required, one can use the purely OSS version of VSCode.

Next consider installing the following VSCode extensions:

 1) https://marketplace.visualstudio.com/items?itemName=arrterian.nix-env-selector
 2) https://marketplace.visualstudio.com/items?itemName=haskell.haskell

These are both open source extensions but we make no claims about their security.  The first allows choosing a nix-environment to run in and the second provides the official Haskell editor support.

With the first plugin, one can hit `ctrl+shift+p` to open the command prompt in VSCode, and then type `nix` and finally choose `select nix shell` from the drop down menu.  Choose our shell `shell.nix` from the menu.  After loading this nix environment all the tools and libraries loaded by nix are available from within the VSCode editor.  After a brief load, VSCode will ask you to restart to load the shell.  Say yes, and let VSCode build the project.  Now things like intelligent autocomplete, type inference on hover, and case completion on holes (via wingman) will work out of the box.

## VSCode in devcontainer [Uses nonOSS]

This project contains a `.devcontainer` folder which contains the project options for opening, editing and building within a docker container.  We have tried to create a complete development environment for this project as a devcontainer, and that includes the Nix tooling for building, the Haskell tooling support for the project, and setting up various pipeline options so that these pieces talk to each other correctly.  Look at the files in `.devcontainer` if desired to change things.  From the devcontainer, one may change the extensions loaded by default, how the container is started and how volumes are loaded for it (the `docker-compose.yml` file), and in fact, any vscode settings.  One can also add or change the custom buttons we included.  Our custom buttons include rebuilding the back and frontends and doing a full compiler build.  VSCode with the Haskell plugin still builds the project on save, but it does not know about bnfc, so these buttons allow performing the stages of the build performed by bnfc.  In general, if the `.cf` files are not changes, one can simply save the file, and cabal will rebuild the project in the background.

For this, one needs the official VSCode Remote - Containers plugin: https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers.  Note: this plugin is not open source, and will not run on the OSS VSCode version without some additional work: https://code.visualstudio.com/docs/remote/faq#_why-arent-the-remote-development-extensions-or-their-components-open-source.  Note that although not OSS, the remote development aspect of VSCode will be free forever.


## Eclipse Theia/Blueprint in container
The eclipse theia and blueprint projects are ways to take the new eclipse core, and create a custom, project-specific ide.  The theia project is also a remote-first solution.  One can host the editor on a server and use over a browser or run the editor locally in their own browser or as a desktop ide (via blueprint).  This also means that one may be able to get the features of VSCode + remote extensions in a fully OSS way.  This should be looked into.

# Buttons
 - Hpack
 - Generate Backend 
 - Generate Frontend
 - Generate compiler -- shouldn't be needed.  Should be able to get away with hpack, generate backend, generate frontend, and save (to trigger cabal builds).
 - Clean
 - Generate graphs (what should this do?)

# Contributing

See our list of project ideas and our list of improvements to the language in the gitlab repository Zim Wiki.  We are currently using Zim wiki software, but may switch to pure markdown wiki in the future.

# Making merge requests 

Note non-mergeable files and folders will appear as "greyed-out" in VSCode.

Merge requests that change devcontainer setup will be considered if they introduce a cleaner flow or introduce better tooling.

# Community policy
An open project must, by its definition, be accessible.  We seek to foster a community of acceptance, inclusion, and collaboration to the fullest extent possible.  We acknowledge that criticism plays a vital role in turning decent ideas into great ones, but please be respectful of others when contributing to this project.  Even if you know the people you are working with try to use positive and respectful language as this helps others (and potential collaborators) that may want to contribute feel that they can safely do so.  No form of harassment will be tolerated within this project.  See our official Regulations for more details.  