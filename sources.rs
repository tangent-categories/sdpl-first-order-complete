fn sigmoid (x : f64) -> f64 where
{
  sin (x)
};
fn neuron1 (x : f64, w : f64, b : f64) -> f64 where
{
  Plus::plus (b, sin (Dot::dot (x, w)))
};
fn g (a : f64, v : f64, w : f64, b : f64) -> f64 where
{
  {
    let x = a;
    Plus::plus (__1neuron1 (x, w, b, Zero::zero(), Zero::zero(), Zero::zero()), (Plus::plus (__1neuron1 (x, w, b, Zero::zero(), Zero::zero(), Zero::zero()), __1neuron1 (x, w, b, Zero::zero(), Zero::zero(), Zero::zero()))))
  }
};
fn __1neuron1 (ax : f64, aw : f64, ab : f64, vx : f64, vw : f64, vb : f64) -> f64 where
{
  {
    let x = ax;
    let w = aw;
    let b = ab;
    Plus::plus (Plus::plus (Zero::zero(), Dot::dot (cos (
    {
      let x = x;
      Dot::dot (x, w)
    }
    ), (Plus::plus (Dot::dot (Zero::zero(), (
    {
      let x = x;
      w
    }
    )), Dot::dot ((
    {
      let x = x;
      x
    }
    ), Zero::zero()))))), (Plus::plus (Plus::plus (Zero::zero(), Dot::dot (cos (
    {
      let w = w;
      Dot::dot (x, w)
    }
    ), (Plus::plus (Dot::dot (Zero::zero(), (
    {
      let w = w;
      w
    }
    )), Dot::dot ((
    {
      let w = w;
      x
    }
    ), Zero::zero()))))), (Plus::plus (Zero::zero(), Zero::zero())))))
  }
};
